

#include <stdint.h>
#include <stdlib.h>
#include "gba.h"
#include "lb_play_gamestate.h"

int main()
{
	/////////////////////////////////////////////////////////////////
	/**
	 *  REGISTERS DEFINITION AND SETUP
	 */
	
	// Set display options.
	// DCNT_MODE0 sets mode 0, which provides two tiled backgrounds.
	// DCNT_BG0 enables background 0.
	// DCNT_BG1 enables background 1.
	// DCNT_BG2 enables background 2.
	// DCNT_BG3 enables background 3.
	// DCNT_OBJ enables objects.
	// DCNT_OBJ_1D make object tiles mapped in 1D (which makes life easier).
	REG_DISPCNT = DCNT_MODE0 | DCNT_BG0 | DCNT_BG1 | DCNT_BG2 | DCNT_OBJ;
	
	// Set backgrounds options.
	// BG_CBB sets the charblock base (where the tiles come from).
	// BG_SBB sets the screenblock base (where the tilemap comes from).
	// BG_8BPP uses 8bpp tiles.
	// BG_REG_32x32 makes it a 32x32 tilemap.
	REG_BG0CNT = BG_CBB(0) | BG_SBB(Const::SB_TEXT) | BG_8BPP | BG_REG_32x32;
	REG_BG1CNT = BG_CBB(1) | BG_SBB(Const::SB_UI) | BG_8BPP | BG_REG_32x32;
	REG_BG2CNT = BG_CBB(1) | BG_SBB(Const::SB_BACKGROUND) | BG_8BPP | BG_REG_32x32;
	
	// Reset horizontal and vertical offsets for each background
	REG_BG0HOFS = 0; REG_BG0VOFS = 0;
	REG_BG1HOFS = 0; REG_BG1VOFS = 0;
	REG_BG2HOFS = 0; REG_BG2VOFS = 0;
	
	
	// Load palette for test map
	LoadPaletteBGData(0, tiles_dungeonPal, sizeof tiles_dungeonPal);
	
	// Play with tiled modes and load them into the memory using the new functions
	LoadPaletteObjData(0, ent_spritesPal, sizeof ent_spritesPal);
	
	// Load the font tiles
	for(int32_t i = 0; i < 128; i ++) {
		LoadTile8(0, i, font_bold[i]); // In corresponding ASCII location num.
	}
	
	
	// Load player tiles from the file directly
	LoadTileData(4, 0, ent_spritesTiles, sizeof ent_spritesTiles);
	
	// Load map tiles from the file
	LoadTileData(1, 0, tiles_dungeonTiles, sizeof tiles_dungeonTiles);
	
	
	// Call clearobjects so that the screen can be updated
	ClearObjects();
	
	
	
	
	/////////////////////////////////////////////////////////////////
	/**
	 *  LOCAL VARIABLES DEFINITION
	 */
	
	BaseState *cState = new PlayState();
	
	
	/////////////////////////////////////////////////////////////////
	/**
	 *  CODE
	 */
	
	cState->loadMedia();
	

	// Main Loop
	while(true) {
		
		cState->handleEvents();
		
		cState->update();

		// Apply graphical changes during VBlank
		WaitVSync();
		
		cState->render();
		
	}

	return 0;
}
	// Check if the current entity is colliding with any other entity
	for(std::vector<Entity*>::iterator ePoolItor = ePool->begin(); ePoolItor != ePool->end(); ePoolItor++) {
		// Check that the entity processed is not the one which we want to resolve collisions with
		if((*ePoolItor) != e) {
		
			// Get unique IDs of the entities
			int32_t eBObjNum = (*ePoolItor)->getObjNum();
			int32_t eAObjNum = e->getObjNum();
			
			// Loop through the already processed entities pool
			for(std::vector<ProcessedEntColl>::iterator procEntPoolItor = procEntPool.begin(); procEntPoolItor != procEntPool.end(); procEntPoolItor++) {
				// If they have already been processed this frame
				if((eAObjNum == procEntPoolItor->eAID && eBObjNum == procEntPoolItor->eBID) ||
					(eBObjNum == procEntPoolItor->eAID && eAObjNum == procEntPoolItor->eBID)) {
					// Just skip the function call if the entities have already been processed
					return;
				}
			}
			
			// Get entity's bounding box. Since its position might change between one collision
			// check and the other, it is necessary to retrieve it every loop
			Rect *eABB = e->getBoundingBox();
				
			// Get entity's bounding box. Since its position might change between one collision
			// check and the other, it is necessary to retrieve it every loop
			Rect *eCBB = (*ePoolItor)->getBoundingBox();

			// If the two entities collide
			if(Rect::checkCollision(*eABB, *eCBB)) {
				
				// Tell entities of the collision between them and let them
				// react accordingly to their properties
				e->handleEntityCollision((*ePoolItor));
				(*ePoolItor)->handleEntityCollision(e);
				
				// Save that these two unique entities have resolved collisions
				// between each other, so that if B has stored a collision event
				// too, nothing will happen
				ProcessedEntColl pec = {eAObjNum, eBObjNum};
				procEntPool.push_back(pec);
			}
		}
	}
}