///-------------------------------------------------------------------------------------------------
// file:	lb_events.h
//
// summary:	Represents events happening into the states, such as the player communicating to
//			spawn the sword
///-------------------------------------------------------------------------------------------------

#ifndef LBEVENTS_H
#define LBEVENTS_H

// Headers
#include "gba.h"
#include <stdint.h>
#include <queue>
#include "lb_constants.h"

// Event to be dispatched
struct Event {
	int32_t eType;
	std::vector<int32_t> data;
	
	Event(int32_t eT, std::vector<int32_t> d) : eType(eT), data(d){};

	Event() : eType(0), data(0){};
	
	Event(int32_t eT) : eType(eT), data(0){};

	Event(const Event &obj) : eType(obj.eType), data(obj.data){};
};

class Events
{
	public:
		static Events* Instance();

		// Add one event to the queue
		void dispatchEvent(Event e);
		
		// Pop one event from the queue
		Event pollEvent();
		
		int32_t getNumEvents();
		
	protected:
		// Constructors
		Events();
		
	private:
		static Events *_instance;
		
		std::queue<Event> *_q;


};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_events.h
///-------------------------------------------------------------------------------------------------