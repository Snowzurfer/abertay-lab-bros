///-------------------------------------------------------------------------------------------------
// file:	lb_entcoll.h
//
// summary:	Implements the entity collision class
///-------------------------------------------------------------------------------------------------

#ifndef LB_ENTCOLL_H
#define LB_ENTCOLL_H

// Entity collisions representation

#include "lb_baseentity.h"
#include <stdint.h>

// Save whether two entities have already been processed
struct ProcessedEntColl {
	int32_t eAID;
	int32_t eBID;
};


class EntityColl {

	public:
		EntityColl(Entity *eA, Entity *eB);
	
		~EntityColl();
		
		
		Entity *getEntA(void) const { return (_eA);};
		Entity *getEntB(void) const { return (_eB);};
		
	private:
	
		// The two entities involved
		Entity *_eA;
		Entity *_eB;
};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_entcoll.h
///-------------------------------------------------------------------------------------------------