///-------------------------------------------------------------------------------------------------
// file:	lb_events_gamestates.cpp
//
// summary:	Implements the state events class
///-------------------------------------------------------------------------------------------------

// Headers
#include "lb_events_gamestates.h"

// Init static reference to singleton object
EventsGameStates* EventsGameStates::_instance = 0;

EventsGameStates* EventsGameStates::Instance() {
	if(_instance == 0) {
		_instance = new EventsGameStates;
	}
	
	return _instance;
}

EventsGameStates::EventsGameStates() {
	
	_q = new std::queue<GameStateEvent>();
}

void EventsGameStates::dispatchEvent(GameStateEvent e) {
	_q->push(e);
}

int32_t EventsGameStates::getNumEvents() {
	return _q->size();
}
		
// Pop one event from the queue
GameStateEvent EventsGameStates::pollEvent() {
	// Check that the queue contains at least one element
	if(_q->size() > 0) {
		GameStateEvent retValue = _q->front();
		_q->pop();
		return retValue;
	}
	else {
		return GameStateEvent(Const::EVENT_NULL,std::vector<int32_t>(0));
	}
}

///-------------------------------------------------------------------------------------------------
// End of lb_events_gamestates.h
///-------------------------------------------------------------------------------------------------