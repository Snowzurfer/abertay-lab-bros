///-------------------------------------------------------------------------------------------------
// file:	room.cpp
//
// summary:	Implements the room class
///-------------------------------------------------------------------------------------------------

// Headers
#include "lb_room.h"

Room::Room() {
	
}

Room::Room(int32_t x, int32_t y, int32_t w, int32_t h) {
	_boxTiles = Rect(x, y, w, h);
	_centrePos = Vector2D(std::floor(static_cast<double>(x + x + w) / 2),
		std::floor(static_cast<double>(y + y + h) / 2));
}

Room::~Room() {

}

bool Room::intersects(Room r) {
	return(Rect::checkCollision(_boxTiles, r.getBoxTiles()));
}




///-------------------------------------------------------------------------------------------------
// End of room.cpp
///-------------------------------------------------------------------------------------------------
