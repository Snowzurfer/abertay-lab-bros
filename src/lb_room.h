///-------------------------------------------------------------------------------------------------
// file:	room.h
//
// summary:	Declares the room class
///-------------------------------------------------------------------------------------------------

#ifndef LBROOM_H
#define LBROOM_H

// Headers
#include <stdint.h>
#include <cmath>
#include "rect.h"
#include "vector_2d.h"

const int32_t minRoomSize = 2;
const int32_t maxRoomSize = 7;

class Room
{
	public:

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Default constructor. </summary>
		///
		/// <remarks>	Alberto, 18/04/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		Room();

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Constructor. </summary>
		///
		/// <remarks>	Alberto, 18/04/2014. </remarks>
		///
		/// <param name="x">	The x pos in tiles. </param>
		/// <param name="y">	The y pos in tiles. </param>
		/// <param name="w">	The width in tiles. </param>
		/// <param name="h">	The height in tiles. </param>
		///-------------------------------------------------------------------------------------------------
		Room(int32_t x, int32_t y, int32_t w, int32_t h);

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Destructor. </summary>
		///
		/// <remarks>	Alberto, 18/04/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		~Room();


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Query if this object intersects the given Room. </summary>
		///
		/// <remarks>	Alberto, 18/04/2014. </remarks>
		///
		/// <param name="r">	The Room to check against. </param>
		///
		/// <returns>	true if it succeeds, false if it fails. </returns>
		///-------------------------------------------------------------------------------------------------
		bool intersects(Room r);

		// Access the BoxTiles
		const Rect& getBoxTiles(void) const		{ return(_boxTiles);	};
		void setBoxTiles(const Rect& boxTiles)	{ _boxTiles = boxTiles;	};

		// Access the CentrePos
		const Vector2D& getCentrePos(void) const		{ return(_centrePos);		};
		void setCentrePos(const Vector2D& centrePos)	{ _centrePos = centrePos;	};

	private:


		/// <summary>	The room's position and size in tiles. </summary>
		Rect _boxTiles;

		/// <summary>	The centre of the room. </summary>
		Vector2D _centrePos; 


		
};

#endif

///-------------------------------------------------------------------------------------------------
// End of room.h
///-------------------------------------------------------------------------------------------------
