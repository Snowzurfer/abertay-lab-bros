/**
 *  lb_camera.cpp
 *  Source file of the Camera class.
 *  
 *  Contains the definition of member functions
 *  
 *  Project: Abertay Lab Bros: Quest For Oculus
 *  Author: Alberto Taiuti, MMXIII 
 */ 
 
// Included libs
#include "lb_camera.h"


Camera::Camera(int32_t x, int32_t y, int32_t w, int32_t h, const Entity *targetEntity) :
				_targetEntity(targetEntity){
	_box = Rect(x, y, w, h);
	_boxTiles = Rect((x / Const::TILE_WIDTH)*2, (y / Const::TILE_HEIGHT)*2, (w / Const::TILE_WIDTH)*2,
					(h / Const::TILE_HEIGHT)*2);
}

Camera::~Camera() {
}

Rect *Camera::getBox() {
	return &_box;
}
Rect *Camera::getBoxTiles() {
	return &_boxTiles;
}

void Camera::update() {
	// Centre camera on target entity
	_box.pos.x = _targetEntity->getPos().x - (_box.w / 2);
	_box.pos.y = _targetEntity->getPos().y - (_box.h / 2);
	
	// Prevent camera from going out of world boundaries
	if(_box.pos.x <= 0) {
		_box.pos.x = 0;
	}
	else if((_box.pos.x + _box.w) >= (MAP_WIDTH_T * Const::TILE_WIDTH)) {
		_box.pos.x = (MAP_WIDTH_T * Const::TILE_WIDTH) - _box.w;
	}
	if(_box.pos.y <= 0) {
		_box.pos.y = 0;
	}
	else if((_box.pos.y + _box.h) >= (MAP_HEIGHT_T * Const::TILE_HEIGHT)) {
		_box.pos.y = (MAP_HEIGHT_T * Const::TILE_HEIGHT) - _box.h;
	}
	
	// Then set the position in tiles of the camera
	_boxTiles.pos.x = (_box.pos.x / Const::TILE_WIDTH)*2;
	_boxTiles.pos.y = (_box.pos.y / Const::TILE_HEIGHT)*2;
	
	
}

void Camera::updateScrolling() {
	// Also set the background offset for a smoother movement effect
	REG_BG2HOFS = _box.pos.x % Const::TILE_WIDTH; REG_BG2VOFS = _box.pos.y % Const::TILE_HEIGHT;
}