/**
 *  lb_tile.cpp
 *  Header file of the Tile class.
 *  
 *  The Tile class represents a "logical" tile: not the one
 *  as represented by the GBA, but as seen by the engine.
 *  The tile is 16x16px, so 4 GBA tiles, each of 8x8.
 *  
 *  Project: Abertay Lab Bros: Quest For Oculus
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#ifndef LBTILE_H
#define LBTILE_H
 
// Included libs
#include "gba.h"
#include "gba_exp.h"
#include "rect.h"
#include <stdint.h>

// The width, in tiles (8x8) of the tileset
const int32_t TILESET_WIDTH_TILES = 16;

//Tile constants
const int32_t TILE_WIDTH = 16;
const int32_t TILE_HEIGHT = 16;
const int32_t TILE_TYPES = 12;

//The different type of tiles
const int32_t TILE_NULL = 0;
const int32_t TILE_BOTTOM = 1;
const int32_t TILE_CENTRE = 2;
const int32_t TILE_FLOOR = 3;
const int32_t TILE_STAIRS = 4;
/*const int32_t TILE_TOP = 11;
const int32_t TILE_TOPRIGHT = 3;
const int32_t TILE_RIGHT = 4;
const int32_t TILE_BOTTOMRIGHT = 5;

const int32_t TILE_BOTTOMLEFT = 7;
const int32_t TILE_LEFT = 8;
const int32_t TILE_TOPLEFT = 9;*/


// Array containing all the instances of logical tile types_
extern const int32_t tileTypes[TILE_TYPES][4];


class Tile
{
	private:
		// Tile's position and size in pixel
		Rect _box;
		
		// Tile's position and size in tiles
		Rect _boxTiles;
		
		// Tile type
		int32_t _type;
		
		
	public:
	
		// Defo constructor
		Tile();
		
		// Copy constructor
		Tile(const Tile &obj);
		
		// Constructor
		Tile(int32_t xPos, int32_t yPos, int32_t tyleType);
		
		// Destructor
		~Tile();
		
		// Render tile
		void render();
		
		// Getters and setters
	
		
		
		int32_t getType();
		Rect &getBox();
		Rect &getBoxTiles();
};

#endif