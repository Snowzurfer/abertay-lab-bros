///-------------------------------------------------------------------------------------------------
// file:	lb_menu_gamestate.h
//
// summary:	Declares the menu state class
///-------------------------------------------------------------------------------------------------

#ifndef MAINMENU_STATE_H
#define MAINMENU_STATE_H

// Headers
#include <stdint.h>
#include <stdlib.h>
#include "gba.h"
#include "font.h"
#include "gba_exp.h"
#include "rect.h"
#include "gba_keys.h"
#include "lb_events.h"
#include "lb_constants.h"
#include "base_state.h"



class MainMenu : public BaseState
{
	public:
		// Constructor
		MainMenu(void);

		// Destructor
		~MainMenu(void);

		// Handle events
		void handleEvents();

		// Update game logic
		void update();


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Renders this object. </summary>
		///
		/// <remarks>	Alberto, 13/04/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		void render();


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Loads the media. </summary>
		///
		/// <remarks>	Alberto, 13/04/2014. </remarks>
		///
		/// <returns>	true if it succeeds, false if it fails. </returns>
		///-------------------------------------------------------------------------------------------------
		bool loadMedia();

	private:
		
		// String buffer for printing text
		char *_stringBuffer;
		
		// Selection in the menu
		int32_t _sel;
		
		// Reference to key handler
		GBAKeys *_keysHandler;

};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_menu_gamestate.h
///-------------------------------------------------------------------------------------------------