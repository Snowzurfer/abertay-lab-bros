///-------------------------------------------------------------------------------------------------
// file:	lb_menu_gamestate.cpp
//
// summary:	Source file for the menustate class
///-------------------------------------------------------------------------------------------------


#include "lb_menu_gamestate.h"


MainMenu::MainMenu(void) {
	
	_stringBuffer = new char[100];
	
	_sel = Const::MENU_SEL_START; 
	
	_keysHandler = GBAKeys::Instance();
}

MainMenu::~MainMenu(void) {
	delete [] _stringBuffer;
}

void MainMenu::handleEvents() {
	// Get the current state of the buttons.
	_keysHandler->updateCurrent();

	// Handle user input
	if(_keysHandler->isBJustPressed(KEY_UP)) {
		_sel --;
	}
	if(_keysHandler->isBJustPressed(KEY_DOWN)) {
		_sel ++;
	}
	if(_keysHandler->isBJustPressed(KEY_A)) {
		// Dispatch change state event
		switch(_sel) {
			case Const::MENU_SEL_START: {
				std::vector<int32_t> args;
				args.push_back(Const::STATE_INGAME);
				eH->dispatchEvent(GameStateEvent(Const::EVENT_CHANGESTATE, args));
				break;
			}
			case Const::MENU_SEL_CREDITS: {
				std::vector<int32_t> args;
				args.push_back(Const::STATE_CREDITS);
				eH->dispatchEvent(GameStateEvent(Const::EVENT_CHANGESTATE, args));
				break;
			}
		}
	}
	
	if(_sel < 0) {
		_sel = 1;
	}
	if(_sel > 1) {
		_sel = 0;
	}
	
	// Update previous buttons variable
	_keysHandler->updatePrevious();
}

// Update is called constantly
void MainMenu::update() {
	
	
}

void MainMenu::render() {

	clearScreenBlock32(Const::SB_TEXT, 0);
	
	// Title
	sprintf(_stringBuffer, "Abertay Lab Bros :");
	drawText(6, 4, _stringBuffer, Const::SB_TEXT);
	sprintf(_stringBuffer, "Quest For Oculus");
	drawText(7, 6, _stringBuffer, Const::SB_TEXT);
	
	// Render the UI
	switch(_sel) {
		case Const::MENU_SEL_START: {
			sprintf(_stringBuffer, "> START <");
			drawText(10, 11, _stringBuffer, Const::SB_TEXT);
			
			sprintf(_stringBuffer, "CREDITS");
			drawText(11, 13, _stringBuffer, Const::SB_TEXT);
			break;
		}
		case Const::MENU_SEL_CREDITS: {
			sprintf(_stringBuffer, "START");
			drawText(12, 11, _stringBuffer, Const::SB_TEXT);
			
			sprintf(_stringBuffer, "> CREDITS <");
			drawText(9, 13, _stringBuffer, Const::SB_TEXT);
			break;
		}
		
	}
	
	
}

// Load assets
bool MainMenu::loadMedia() {

	clearScreenBlock32(Const::SB_BACKGROUND, 1);
}

///-------------------------------------------------------------------------------------------------
// End of base_state.cpp
///-------------------------------------------------------------------------------------------------