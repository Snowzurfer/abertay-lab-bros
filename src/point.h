/**
 *  point.h
 *  Header file of Point class.
 *  
 *  The point class represents a geometric point, and contains
 *  useful functions for manipulating it.
 *  
 *  Project: Abertay Lab Bros: Quest For Oculus
 *  Author: Alberto Taiuti, MMXIII 
 */

#ifndef POINT_H
#define POINT_H

#include <stdint.h>

// Included libs

// Class definition

class Point
{
	public:
		
		//Default constructor
		Point();
		
		// 2nd constructor
		Point(int32_t x, int32_t y);
		
		// Default deconstructor
		~Point(); 

		int32_t x, y; // Position of the point
		
		// Rotate around the origin by a given angle (angle)
		//svoid rotate2D(int32_t angle);
		
		// Set a new position (default: same as before)
		void setPos(int32_t new_x, int32_t new_y);
		
		// Increment (or decrement) the position by x_step, y_step
		void addToPos(int32_t x_step, int32_t y_step);		
};

#endif