/**
 *  lb_camera.h
 *  Header file of the Camera class.
 *  
 *  The camera class is used for scrolling the screen
 *  around.
 *  
 *  Project: Abertay Lab Bros: Quest For Oculus
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#ifndef LBCAM_H
#define LBCAM_H
 
// Included libs
#include "gba.h"
#include "rect.h"
#include "lb_baseentity.h"
#include "lb_dungeon.h"
#include "lb_tile.h"


class Camera
{
	private:
		// Camera's shape and position
		Rect _box;
		Rect _boxTiles; // Tile units version
		
		// Entity to follow
		const Entity *const _targetEntity;
		
	public:
		
		// Constructor
		Camera(int32_t x, int32_t y, int32_t w, int32_t h, const Entity *targetEntity);
		// Destructor
		~Camera();
		
		// Update the camera's position relative to the player
		void update();
		
		// Scroll the background depending on the camera's position
		void updateScrolling();
		
		// Getters and setters
		Rect *getBox();
		Rect *getBoxTiles();
};

#endif