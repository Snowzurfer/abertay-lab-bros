///-------------------------------------------------------------------------------------------------
// file:	lb_events_gamestates.h
//
// summary:	Used to request the engine to change state. Usually used inside the child states
///-------------------------------------------------------------------------------------------------

#ifndef LBEVENTSGS_H
#define LBEVENTSGS_H

// Headers
#include "gba.h"
#include <stdint.h>
#include <queue>
#include "lb_constants.h"

// Event to be dispatched
struct GameStateEvent {
	int32_t eType;
	std::vector<int32_t> data;

	GameStateEvent(int32_t eT, std::vector<int32_t> d) : eType(eT), data(d){};

	GameStateEvent() : eType(0), data(0){};
	
	GameStateEvent(int32_t eT) : eType(eT), data(0){};

	GameStateEvent(const GameStateEvent &obj) : eType(obj.eType), data(obj.data){};
};

class EventsGameStates
{
	public:
		static EventsGameStates* Instance();

		// Add one event to the queue
		void dispatchEvent(GameStateEvent e);
		
		// Pop one event from the queue
		GameStateEvent pollEvent();
		
		int32_t getNumEvents();
		
	protected:
		EventsGameStates();
		
	private:
		static EventsGameStates *_instance;
		
		std::queue<GameStateEvent> *_q;


};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_events_gamestates.h
///-------------------------------------------------------------------------------------------------