# Makefile for AG070 projects.
#
# Edited by Alberto Taiuti. 
# 
# 	Added directory layout; 
# 	changed TARGET name;
#	added LIBS macro;


# Use the ARM C++ toolchain.
CC = arm-none-eabi-g++
OBJCOPY = arm-none-eabi-objcopy

# Specify project directory layout. Makefile included into the src dir
IDIR = ../include
ODIR = obj
LDIR = ../lib
BDIR = ../bin
ADIR = ../assets

# Macro for included vendor or std libraries
LIBS = -lm

# Compiler options:
# -g      Enable debugging symbols
# -O3     Full compiler optimisations
# -Wall   Enable all warnings
CFLAGS = -g -O3 -Wall

# Architecture options for the GBA.
MODEL = -mthumb -mthumb-interwork -specs=gba_mb.specs

# The name of the program to build.
TARGET = LabBros

# The object files to link into the final program.
# The rules below will build foo.o from foo.cpp automatically.
_OBJECTS = \
	main.o \
	gba.o \
	font.o \
	gba_exp.o \
	point.o \
	rect.o \
	tiles_dungeon.o \
	lb_tile.o \
	lb_camera.o \
	lb_room.o \
	lb_dungeon.o \
	lb_baseentity.o \
	vector_2d.o \
	lb_player.o \
	gba_keys.o \
	lb_events.o \
	lb_enemyA.o \
	lb_entcoll.o \
	lb_sword.o \
	lb_anim.o \
	ent_sprites.o \
	lb_constants.o \
	lb_enemyB.o \
	lb_play_gamestate.o \
	base_state.o \
	lb_menu_gamestate.o \
	lb_events_gamestates.o \
	lb_credits_gamestate.o \
	lb_gameover_gamestate.o
	
	
	
OBJECTS = $(addprefix $(ODIR)/,$(_OBJECTS))

# The default target to make if none is specified.
all: $(TARGET).gba

# Convert a .elf file into a .gba ROM image.
%.gba: %.elf
	$(OBJCOPY) -O binary $< $@
	gbafix $@

# Link .o files together to produce a .elf file.
$(BDIR)/$(TARGET).elf: $(OBJECTS)
	$(CC) $(MODEL) -o $@ $^ -L $(LDIR) $(LIBS)

# Compile a .cpp file into a .o file, into the respective dir.
$(ODIR)/%.o: %.cpp
	$(CC) $(CFLAGS) $(MODEL) -c -o $@ $<
	
# Compile a .c file into a .o file, into the respective dir.
$(ODIR)/%.o: %.c
	$(CC) $(CFLAGS) $(MODEL) -c -o $@ $<

# Run the ROM image using VBA.
run: $(BDIR)/$(TARGET).gba
	VisualBoyAdvance $^

# Run a program with a different value of TARGET.
# (This lets you have multiple GBA programs in the same directory;
# if you have foo.cpp, you can compile and run it with "make run-foo".)
run-%:
	@$(MAKE) run TARGET=$*

# Remove all the compiled files.	
clean:
	rm -f $(ODIR)/*.gba *.elf *.o depend.mk

# Automatically extract dependencies from .cpp files.
# Invoking the compiler with -MM below makes it scan the source files and
# write out Makefile-style rules saying which other files they depend upon;
# we then include that file.
-include depend.mk
depend.mk: *.cpp *.h
	$(CC) -MM *.cpp >depend.mk
