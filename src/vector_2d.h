/**
 *  Vector2D.h
 *  Header file of Vector2D class.
 *  
 *  The Vector2D class represents a 2D vector and
 *  it is much similar to the Point class, with the
 *  difference that it is treated and developed as a proper vector.
 *  Moreover, it contains methods inherent to vector maths.
 */ 

#ifndef VECTOR2D_H
#define VECTOR2D_H

// Included libs
#include <cmath>
#include <stdint.h>

// Class definition
class Vector2D
{
	public:

		///-------------------------------------------------------------------------------------------------
		/// <summary>	Default constructor. </summary>
		///
		/// <remarks>	Alberto, 12/04/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		Vector2D();


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Constructor. </summary>
		///
		/// <remarks>	Alberto, 12/04/2014. </remarks>
		///
		/// <param name="x">	The x coordinate. </param>
		/// <param name="y">	The y coordinate. </param>
		///-------------------------------------------------------------------------------------------------
		Vector2D(int32_t x, int32_t y); 
		
		// Copy constructor
		Vector2D(const Vector2D &cVA);
		
		// Default destructor
		~Vector2D(); 
		
		int32_t x, y; // X(i) and Y(j) components
		int32_t magnitude; // Magnitude of the vector	
		int32_t angle; // Angle of vector with the X-axis
		
		// Rotate around the origin by a given angle (angle)
		void rotate2D(int32_t angle);
		
		// Set new positions (default: same as before)
		void setPos(int32_t new_x, int32_t new_y);
		
		// Increment (or decrement) the components by x_step, y_step
		void addToPos(int32_t x_step, int32_t y_step);
		
		// Calculate the dot product between current vector and another vector (b)
		int32_t dotProduct(const Vector2D &b) const;

		// Calculate angle between two vectors
		static int32_t angleBetween(const Vector2D &a, const Vector2D &b);
		
		// Normalize the current vector
		void normalize();
		
		// Transform the current vector to its left normal
		void makeNormalLeft();		
		
		// Op overloading
		friend Vector2D operator+(const Vector2D &xVA, const Vector2D &cVB);
		Vector2D& operator= (const Vector2D &cVA);
		
};



#endif

///-------------------------------------------------------------------------------------------------
// End of vector_2d.h
///-------------------------------------------------------------------------------------------------