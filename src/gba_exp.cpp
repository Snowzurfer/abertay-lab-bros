///-------------------------------------------------------------------------------------------------
// file:	gba_exp.cpp
//
// summary:	Contains utility functions
///-------------------------------------------------------------------------------------------------

// Headers
#include "gba_exp.h"



void drawText(int32_t x, int32_t y, const char string[], int32_t sBlock) {
	// Loop until the terminator is found
	for(int i = 0; string[i] != '\0'; i ++) {
		SetTile(sBlock,(x + i), y, string[i]);
	}
}

void drawText(int32_t x, int32_t y, int32_t integer, int32_t sBlock) {
	// Use a buffer for converting the number into text
	char buf[32];
	// Convert the integer to string
	sprintf(buf, "%d", integer);
	
	// Loop until the terminator is found
	for(int i = 0; buf[i] != '\0'; i ++) {
		SetTile(sBlock,(x + i), y, buf[i]);
	}
}

int32_t sigNum(int32_t x) {
	return (x > 0) - (x < 0);
}

void clearScreenBlock32(int32_t sBlock, int32_t tile){
	for(int i = 0; i < 32; i ++) {
		for(int j = 0; j < 32; j ++) {
			SetTile(sBlock, i, j, tile);
		}
	}
}

///-------------------------------------------------------------------------------------------------
// End of gba_exp.cpp
///-------------------------------------------------------------------------------------------------