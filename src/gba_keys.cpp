///-------------------------------------------------------------------------------------------------
// file:	gba_keys.cpp
//
// summary:	Source code for GBAKeys class
///-------------------------------------------------------------------------------------------------


// Headers
#include "gba_keys.h"

// Init static reference to singleton object
GBAKeys* GBAKeys::_instance = 0;

GBAKeys* GBAKeys::Instance() {
	if(_instance == 0) {
		_instance = new GBAKeys;
	}
	
	return _instance;
}

GBAKeys::GBAKeys() {
	_currKeys = 0xFFFF;
	_prevKeys = 0xFFFF;
}

bool GBAKeys::isBPressed(uint16_t key) {
	if((_currKeys & key) == 0) {
		return true;
	}
	else {
		return false;
	}
}

bool GBAKeys::isBJustPressed(uint16_t key) {
	// If the button is pressed now, but before was not
	if(((_currKeys & key) == 0) && 
		((_prevKeys & key) != 0)) {
			return true;
		}
	else {
		return false;
	}
}

// Update current keys
void GBAKeys::updateCurrent() {
	_currKeys = REG_KEYINPUT;
}
		
// Update previous keys
void GBAKeys::updatePrevious() {
	_prevKeys = _currKeys;
}

///-------------------------------------------------------------------------------------------------
// End of gba_keys.cpp
///-------------------------------------------------------------------------------------------------