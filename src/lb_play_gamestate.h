///-------------------------------------------------------------------------------------------------
// file:	lb_play_gamestate.h
//
// summary:	Declares the ingame state class. This is where most of the game is run.
///-------------------------------------------------------------------------------------------------

#ifndef INGAME_STATE_H
#define INGAME_STATE_H

// Headers
#include <stdint.h>
#include <stdlib.h>
#include "gba.h"
#include "font.h"
#include "gba_exp.h"
#include "lb_dungeon.h"
#include "tiles_dungeon.h"
#include "lb_tile.h"
#include "lb_camera.h"
#include "rect.h"
#include "gba_keys.h"
#include "lb_player.h"
#include "lb_events.h"
#include "lb_enemyA.h"
#include <queue>
#include "lb_entcoll.h"
#include "lb_sword.h"
#include "ent_sprites.h"
#include "lb_enemyB.h"
#include "lb_constants.h"
#include "base_state.h"
#include "lb_events_gamestates.h"



class PlayState : public BaseState
{
	public:

		// Constructor
		PlayState(void);

		// Destructor
		~PlayState(void);

		// Handle input
		void handleEvents();

		// Update logic
		void update();

		// Render
		void render();

		// Load media 
		bool loadMedia();

	private:
		
		// Seed used for creating new dungeons at run-time
		int32_t dunSeed;
		// Counter of difficulty for current level
		int32_t dunDiff;
		
		// String buffer for printing text
		char *stringBuffer;
		
		// Pointer to pool of entities which will be created during dungeon generation
		//Entity **ePool = NULL;
		std::vector<Entity*> *ePool;
		std::vector<Entity*>::iterator ePoolItor;
		
		// Player entity
		Player *player;
		
		// Ref to sword (for when it is alive, being created)
		Sword *sword;
		
		// The dungeon map handler
		Dungeon *dunManager;
		
		// Camera
		Camera *cam;
		
		// Queue of entity-entity collisions
		std::queue<EntityColl> entCollPool;
		
		// Reference to events handler (singleton pattern; ugly, but quick way to make it work)
		Events *eH;
		
		// Reference to game states events handler
		EventsGameStates *eHStates;
		
		// Manage entity-tile collisions
		void _checkAndResolveCollisionsMap(Entity *e, int32_t *dunMap);

		// After having managed collisions with the map, manage collisions entity-entity
		void _checkAndResolveCollisionsEntities(Entity* e, std::vector<Entity*> *ePool, std::vector<ProcessedEntColl> &procEntPool);
};

#endif

