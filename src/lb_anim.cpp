// source for anim

// Headers
#include "lb_anim.h"


Anim::Anim(int32_t framesNum, int32_t objNum, bool flipV, bool flipH, int32_t framesTreshold)
			:  _framesNum(framesNum), _objNum(objNum), _flipV(flipV), _flipH(flipH), _framesTreshold(framesTreshold) {
	_framesPos = new int32_t[framesNum];
	
	_changeFrame = false;
	
	_currAnimFrame = 0;
}

Anim::~Anim() {
	delete[] _framesPos;
}

void Anim::update() {
	++_framesCounter;
	
	// If it is time to change frame
	if(_framesCounter % _framesTreshold == 0) {
		// Update animation frames counter
		++_currAnimFrame;
		
		_framesCounter = 0;
		
		_changeFrame = true;
	}
	
	// Cap animation frames
	if(_currAnimFrame >= _framesNum) {
		_currAnimFrame = 0;
	}
}

void Anim::draw() {
	// Set OAM only if there is actual need to change frame, not every loop
	if(_changeFrame) {
		setObjectTileIndex(_objNum, *(_framesPos + _currAnimFrame));
		setObjectFlipY(_objNum, _flipH);
		setObjectFlipX(_objNum, _flipV);
		_changeFrame = false;
	}
}

void Anim::pushFrame(int32_t framePos) {
	// Static variable to keep track of the number of elements inserted 
	static int32_t _numElements = 0;
	
	*(_framesPos + _numElements) = framePos;
	
	++ _numElements;
}


///-------------------------------------------------------------------------------------------------
// End of room.cpp
///-------------------------------------------------------------------------------------------------
