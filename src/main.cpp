///-------------------------------------------------------------------------------------------------
// file:	play.cpp
//
// summary:	Implements the ingame state class
///-------------------------------------------------------------------------------------------------


#include <stdint.h>
#include <stdlib.h>
#include "gba.h"
#include "lb_play_gamestate.h"
#include "lb_menu_gamestate.h"
#include "lb_events_gamestates.h"
#include "lb_credits_gamestate.h"
#include "lb_gameover_gamestate.h"


int main()
{
	/////////////////////////////////////////////////////////////////
	/**
	 *  REGISTERS DEFINITION AND SETUP
	 */
	
	// Set display options.
	// DCNT_MODE0 sets mode 0, which provides two tiled backgrounds.
	// DCNT_BG0 enables background 0.
	// DCNT_BG1 enables background 1.
	// DCNT_BG2 enables background 2.
	// DCNT_BG3 enables background 3.
	// DCNT_OBJ enables objects.
	// DCNT_OBJ_1D make object tiles mapped in 1D (which makes life easier).
	REG_DISPCNT = DCNT_MODE0 | DCNT_BG0 | DCNT_BG1 | DCNT_BG2 | DCNT_OBJ;
	
	// Set backgrounds options.
	// BG_CBB sets the charblock base (where the tiles come from).
	// BG_SBB sets the screenblock base (where the tilemap comes from).
	// BG_8BPP uses 8bpp tiles.
	// BG_REG_32x32 makes it a 32x32 tilemap.
	REG_BG0CNT = BG_CBB(0) | BG_SBB(Const::SB_TEXT) | BG_8BPP | BG_REG_32x32;
	REG_BG1CNT = BG_CBB(1) | BG_SBB(Const::SB_UI) | BG_8BPP | BG_REG_32x32;
	REG_BG2CNT = BG_CBB(1) | BG_SBB(Const::SB_BACKGROUND) | BG_8BPP | BG_REG_32x32;
	
	// Reset horizontal and vertical offsets for each background
	REG_BG0HOFS = 0; REG_BG0VOFS = 0;
	REG_BG1HOFS = 0; REG_BG1VOFS = 0;
	REG_BG2HOFS = 0; REG_BG2VOFS = 0;
	
	
	// Load palette for test map
	LoadPaletteBGData(0, tiles_dungeonPal, sizeof tiles_dungeonPal);
	
	// Play with tiled modes and load them into the memory using the new functions
	LoadPaletteObjData(0, ent_spritesPal, sizeof ent_spritesPal);
	
	// Load the font tiles
	for(int32_t i = 0; i < 128; i ++) {
		LoadTile8(0, i, font_bold[i]); // In corresponding ASCII location num.
	}
	
	
	// Load player tiles from the file directly
	LoadTileData(4, 0, ent_spritesTiles, sizeof ent_spritesTiles);
	
	// Load map tiles from the file
	LoadTileData(1, 0, tiles_dungeonTiles, sizeof tiles_dungeonTiles);
	
	
	// Call clearobjects so that the screen can be updated
	ClearObjects();
	
	
	
	
	/////////////////////////////////////////////////////////////////
	/**
	 *  LOCAL VARIABLES DEFINITION
	 */
	
	BaseState *cState = new MainMenu();
	
	EventsGameStates *eH = EventsGameStates::Instance();
	
	
	/////////////////////////////////////////////////////////////////
	/**
	 *  CODE
	 */
	
	// Load media of first state being used
	cState->loadMedia();
	

	// Main Loop
	while(true) {
		
		// Handle events for current state
		cState->handleEvents();
		
		// Update current state
		cState->update();
		
		// Loop through the game events and act accordingly
		GameStateEvent e = eH->pollEvent();
		while(e.eType != Const::EVENT_NULL) {
			switch(e.eType) {
				case Const::EVENT_CHANGESTATE: {
				
					// Clear OAM
					ClearObjects();
					
					// Clear screenblocks
					clearScreenBlock32(Const::SB_TEXT, 0);
					clearScreenBlock32(Const::SB_UI, 0);
					clearScreenBlock32(Const::SB_BACKGROUND, 0);

					// Delete current state and instantiate the
					// new one
					switch((e.data)[0]) {
						case Const::STATE_INGAME: {
							delete cState;
							cState = new PlayState();
							break;
						}
						case Const::STATE_CREDITS: {
							delete cState;
							cState = new Credits();
							break;
						}
						case Const::STATE_GAMEOVER: {
							delete cState;
							cState = new GameOver((e.data)[1], (e.data)[2]);
							break;
						}
						case Const::STATE_MENU: {
							delete cState;
							cState = new MainMenu();
							break;
						}
					}
					
					// Load media for the newly instantiated state
					cState->loadMedia();
					
					break;
				}
			
			}
			
			// Retrieve the next event in the queue
			e = eH->pollEvent();
		}

		// Apply graphical changes during VBlank
		WaitVSync();
		
		// Render current state
		cState->render();
	}

	return 0;
}


