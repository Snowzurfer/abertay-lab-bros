///-------------------------------------------------------------------------------------------------
// file:	lb_enemyB.cpp
//
// summary:	Implements the enemyB class
///-------------------------------------------------------------------------------------------------

// Headers
#include "lb_enemyB.h"

EnemyB::EnemyB(int32_t xPos, int32_t yPos, int32_t health): 
				Entity(xPos, yPos, 1, ENT_ENEMY_B),
				_hurtTimerTreshold(60), // Hurt
				_moveAnimTimerThreshold(10), // Moving anim
				_graphicsPos(Vector2D(xPos - 8, yPos - 8)){ // Pos of graphs
				
	_currState = STATE_MOVING;
	
	_moveRight = true;
	
	
	_hurtTimerCounter = 0;
	
	_health = health;
	
	++ objNumCounter;
	
	_score = 5;
}

EnemyB::~EnemyB() {
	//delete _spriteTileNum;
	//_spriteTileNum = NULL;
	//delete _anim;
}

void EnemyB::handleEntityCollision(Entity *e) {
	// React differently depending on type of entity
	switch(e->getType()) {
		// When collides with player, the entity just turns back
		case ENT_PLAYER: {
			// Get entity's bounding box. Since its position might change between one collision
			// check and the other, it is necessary to retrieve it every loop
			Rect *eABB = getBoundingBox();
				
			// Get entity's bounding box. Since its position might change between one collision
			// check and the other, it is necessary to retrieve it every loop
			Rect *eBBB = e->getBoundingBox();
			
			// Get the sign of the velocity of the entity
			int32_t xVelSigNum = sigNum(_velocity.x);
			int32_t yVelSigNum = sigNum(_velocity.y);
			
			// Get the intersection rectangle (area between the two entities)
			int32_t x = fmax(eABB->pos.x, eBBB->pos.x);
			int32_t y = fmax(eABB->pos.y, eBBB->pos.y);
			int32_t w = fmin(eABB->pos.x + eABB->w, eBBB->pos.x + eBBB->w);
			int32_t h = fmin(eABB->pos.y + eABB->h, eBBB->pos.y + eBBB->h);
			w-=x;
			h-=y;
			
			// Calculate new desired position of the entity
			int32_t xPos = _desPos.x - (w * xVelSigNum);
			int32_t yPos = _desPos.y - (h * yVelSigNum);
			
			// If the intersection rectangle is wider than taller
			if(w > h) { 
				// Resolve vertically	
				setDesPos(Vector2D(_desPos.x, yPos));	
			}
			// If the intersection rectangle is taller than wider
			else {
				// Resolve horizontally	
				setDesPos(Vector2D(xPos, _desPos.y));
			}
			
			// Invert the current direction
			_changeDir();
			
			break;
		}
		
		// When an enemy gets hit by the sword
		case ENT_SWORD: {
			if (_currState != STATE_HURT) {
				// Change state to hurt
				_prevState = _currState;
				_currState = STATE_HURT;
				
				// Reduce health
				--_health;
				
				// Stop the entity
				_stopMoving();
				
				// Reset timer
				_hurtTimerCounter = 0;
			}
		}
	}
}

void EnemyB::draw(const Vector2D &camOffset) {
	_graphicsPos.x = (_pos.x - 8) - camOffset.x;
	_graphicsPos.y = (_pos.y - 8) - camOffset.y;
	
	// The entity is outside the boundaries of the screen
	if(_graphicsPos.x >= Const::RENDERAREA_W ||
		_graphicsPos.x <= 0 || 
		_graphicsPos.y >= Const::RENDERAREA_H ||
		_graphicsPos.y <= 0) {
		// Hide it
		if(_visible) {
			setObjectVisibility(_objNum, false);
			_visible = false;
		}
		setObjectVisibility(_objNum, false);
	}
	else {
		if(!_visible) {
			setObjectVisibility(_objNum, true);
			_visible = true;
		}
		
		// Set sprite's position relative to the camera
		SetObjectX(_objNum, _graphicsPos.x);
		SetObjectY(_objNum, _graphicsPos.y);
		
		// Set the correct frame (eventual flipping happens in update)
		setObjectTileIndex(_objNum, _currAnimFrame);
	}
}

void EnemyB::update() {
	
	switch(_currState) {
		case STATE_MOVING: {			
			if (_moveUp && _moveDown) {
				// do nothing
			}
			else if (_moveUp) {
				_velocity.y = -_maxVel;
			}
			else if (_moveDown) {
				_velocity.y = _maxVel;
			}
			
			
			if (_moveLeft && _moveRight) {
				// do nothing
			}
			else if (_moveLeft) {
				_velocity.x = -_maxVel;
			}
			else if (_moveRight) {
				_velocity.x = _maxVel;
			}
			
			
			
			// Update anim
			
			// Inc counter
			++ _movAnimCounter;
			
			// If counter reached threshold
			if(_movAnimCounter % _moveAnimTimerThreshold == 0) {
				// Reset counter
				_movAnimCounter = 0;
				
				// Move to next frame
				switch(_currDir) {
					case DIR_NORTH: {
						// Choose next frame depending on current one
						if(_currAnimFrame == Const::FRM_ENEMYB_N_WALK0) {
							_currAnimFrame = Const::FRM_ENEMYB_N_WALK1;
						}
						else {
							_currAnimFrame = Const::FRM_ENEMYB_N_WALK0;
						}
						
						break;
					}
					case DIR_EAST: {
						// Choose next frame depending on current one
						if(_currAnimFrame == Const::FRM_ENEMYB_E_WALK0) {
							_currAnimFrame = Const::FRM_ENEMYB_E_WALK1;
						}
						else {
							_currAnimFrame = Const::FRM_ENEMYB_E_WALK0;
						}
						
						setObjectFlipY(_objNum, false);
						
						break;
					}
					case DIR_WEST: {
						// Choose next frame depending on current one
						if(_currAnimFrame == Const::FRM_ENEMYB_W_WALK0) {
							_currAnimFrame = Const::FRM_ENEMYB_W_WALK1;
						}
						else {
							_currAnimFrame = Const::FRM_ENEMYB_W_WALK0;
						}
						
						setObjectFlipY(_objNum, true);
						
						break;
					}
					case DIR_SOUTH: {
						// Choose next frame depending on current one
						if(_currAnimFrame == Const::FRM_ENEMYB_S_WALK0) {
							_currAnimFrame = Const::FRM_ENEMYB_S_WALK1;
						}
						else {
							_currAnimFrame = Const::FRM_ENEMYB_S_WALK0;
						}
						
						break;
					}
				}
			}
		}
		case STATE_HURT: {
			// Update timer
			++_hurtTimerCounter;
			
			
			// Animate (make entity flicker)
			
			
			// Check for threshold for finishing hurt routine
			if(_hurtTimerCounter >= _hurtTimerTreshold) {
				// Exit this state and set it to moving
				_currState = STATE_MOVING;
				
				// Reset timer
				_hurtTimerCounter = 0;
				
				// Start moving again in previous direction
				switch(_currDir) {
					case DIR_NORTH: {
						_moveUp = true;
						break;
					}
					case DIR_EAST: {
						_moveRight = true;
						break;
					}
					case DIR_WEST: {
						_moveLeft = true;
						break;
					}
					case DIR_SOUTH: {
						_moveDown = true;
						break;
					}
				}
			}
			break;
		}	
	}
	
	// Update base class
	Entity::update();
}

void EnemyB::_changeDir() {
	// Depending on current direction
	switch(_currDir) {
		case DIR_NORTH: {
			setOneDir(DIR_SOUTH);
			break;
		}
		case DIR_EAST: {
			setOneDir(DIR_WEST);
			break;
		}
		case DIR_WEST: {
			setOneDir(DIR_EAST);
			break;
		}
		case DIR_SOUTH: {
			setOneDir(DIR_NORTH);
			break;
		}
	}
}

void EnemyB::handleTileCollision(int32_t tileType, int32_t tileIndex, int32_t w, int32_t h) {

	// Act differently depending on the type of tile
	switch(tileType) {
		default: {
			
			// Act differently depending on the index of tile being computed
			switch(tileIndex) {
				case 0: { // Tile beneath the entity
					setDesPos(Vector2D(_desPos.x, _desPos.y - h));
					setOneDir(DIR_WEST);
					break;
				}
				case 1: { // Tile above entity
					setDesPos(Vector2D(_desPos.x, _desPos.y + h));
					setOneDir(DIR_EAST);
					break;
				}
				case 2: { // Tile left of entity
					setDesPos(Vector2D(_desPos.x + w, _desPos.y));
					setOneDir(DIR_NORTH);
					break;
				}
				case 3: { // Tile right of entity
					setDesPos(Vector2D(_desPos.x - w, _desPos.y));
					setOneDir(DIR_SOUTH);
					break;
				}
				default: { // Handle diagonal tiles cases separately
					// If the intersection rectangle is wider than taller
					if(w > h) { 
						// Resolve vertically
						setOneDir(DIR_EAST);
						
						// If the tile is beneath the entity
						if(tileIndex > 5) {
							setDesPos(Vector2D(_desPos.x, _desPos.y - h));
						}
						// If the tile is above the entity
						else {
							setDesPos(Vector2D(_desPos.x, _desPos.y + h));
						}
					}
					// If the intersection rectangle is taller than wider
					else {
						// Resolve horizontally
						setOneDir(DIR_SOUTH);
						
						// If the tile is left to the entity
						if(tileIndex == 6 || tileIndex == 4){
							setDesPos(Vector2D(_desPos.x + w, _desPos.y));
						}
						// If the tile is right to the entity
						else {
							setDesPos(Vector2D(_desPos.x - w, _desPos.y));
						}
					}
				}
			}
		}
	}
}

///-------------------------------------------------------------------------------------------------
// End of lb_enemyB.cpp
///-------------------------------------------------------------------------------------------------