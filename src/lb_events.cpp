///-------------------------------------------------------------------------------------------------
// file:	lb_events.cpp
//
// summary:	Implements the events class
///-------------------------------------------------------------------------------------------------

// Headers
#include "lb_events.h"

// Init static reference to singleton object
Events* Events::_instance = 0;

Events* Events::Instance() {
	if(_instance == 0) {
		_instance = new Events;
	}
	
	return _instance;
}

Events::Events() {
	
	_q = new std::queue<Event>();
}

void Events::dispatchEvent(Event e) {
	_q->push(e);
}

int32_t Events::getNumEvents() {
	return _q->size();
}
		
// Pop one event from the queue
Event Events::pollEvent() {
	// Check that the queue contains at least one element
	if(_q->size() > 0) {
		Event retValue = _q->front();
		_q->pop();
		return retValue;
	}
	else {
		return Event(-1,std::vector<int32_t>(0));
	}
}

///-------------------------------------------------------------------------------------------------
// End of lb_events.cpp
///-------------------------------------------------------------------------------------------------
