/**
 *  lb_tile.cpp
 *  Source file of the Tile class.
 *  
 *  Contains the definition of member functions
 *  
 *  Project: Abertay Lab Bros: Quest For Oculus
 *  Author: Alberto Taiuti, MMXIII 
 */ 
 
 
 // CHANGE WITH ABSTRACT FACTORY IF POSSIBLE
 
 
// Included libs
#include "lb_tile.h"
 
 
const int32_t tileTypes[TILE_TYPES][4] = {
	{
		22,23,30,31,
	},
	{
		2,3,10,11,
	},
	{
		4,5,12,13,
	},
	{
		6,7,14,15 
	},
	{
		20,21,28,29,
	},

	{
		6,7,14,15,
	},
	{
		22,23,30,31,
	},
	{
		38,39,46,47,
	},
	{
		36,37,44,45,
	},
	{
		34,35,42,43,
	},

	{
		18,19,26,27,
	},
	{
		2,3,10,11,
	},
};

 
Tile::Tile() {

}

Tile::Tile(const Tile &obj) {
	_box = obj._box;
	_type = obj._type;
	_boxTiles = obj._boxTiles;
}

Tile::Tile(int32_t xPos, int32_t yPos, int32_t tyleType) {
	_box = Rect(xPos, yPos, TILE_WIDTH, TILE_HEIGHT);
	_type = tyleType;
	_boxTiles = Rect(xPos / 8, yPos / 8, 1, 1);
}

// Destructor
Tile::~Tile() {
}

// Render tile
void Tile::render() {
	
}


// Getters and setters
int32_t Tile::getType() {
	return _type;
}
Rect &Tile::getBox() {
	return _box;
}
Rect &Tile::getBoxTiles() {
	return _boxTiles;
}