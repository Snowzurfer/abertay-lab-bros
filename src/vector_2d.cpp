///-------------------------------------------------------------------------------------------------
// file:	vector_2d.cpp
//
// summary:	Implements the vector 2d class
///-------------------------------------------------------------------------------------------------

// Included libs
#include "vector_2d.h"


Vector2D::Vector2D() {
	this->x = 0; this->y = 0;
	magnitude = 0;
}

Vector2D::Vector2D(int32_t x, int32_t y)
{
	this->x = x; this->y = y;
	
	// Calculate the magnitude
	magnitude = sqrt((x*x) + (y*y));
}

Vector2D::Vector2D(const Vector2D& cVA) {
	x = cVA.x; y = cVA.y;
	magnitude = cVA.magnitude;
}

Vector2D::~Vector2D(){}

void Vector2D::rotate2D(int32_t angle)
{
	// Store the trigs of the angle, to save CPU (they won't be computed again)
	int32_t cos_angle = cos(int32_t(angle));
	int32_t sin_angle = sin(int32_t(angle));
	
	// Rotate the direction around the origin
	int32_t temp_x = x*cos_angle - y*sin_angle;
	int32_t temp_y = x*sin_angle + y*cos_angle;
	
	// Assign values back
	x = temp_x; y = temp_y;
	
	// Change angle
	this->angle += angle;
}

void Vector2D::setPos(int32_t new_x, int32_t new_y) {
	this->x = new_x; this->y = new_y;	
}

void Vector2D::addToPos(int32_t x_step, int32_t y_step) {
	// Add values
	x += x_step; y += y_step;
}

int32_t Vector2D::dotProduct(const Vector2D &b) const {
	return ((x * b.x) + (y * b.y));
}

void Vector2D::normalize() {
	if (magnitude != 0.0)
	{
		x = x / magnitude;
		y = y / magnitude;
	}
}

void Vector2D::makeNormalLeft() {
	// Save the original x (i) component
	int32_t temp_x = x;
	// Perform the change
	x = y;
	y =  -1 * temp_x;	
}

int32_t Vector2D::angleBetween(const Vector2D &a, const Vector2D &b) {
	int32_t cosAlpha = ((a.dotProduct(b)) / ((a.magnitude)*(b.magnitude)));

	return acos(cosAlpha);
}

Vector2D operator+(const Vector2D &cVA, const Vector2D &cVB) {
	int32_t x = cVA.x + cVB.x;
	int32_t y = cVA.y + cVB.y;
	
	return Vector2D(x, y);
}

Vector2D& Vector2D::operator= (const Vector2D &cVA) {
	x = cVA.x; y = cVA.y;
	magnitude = cVA.magnitude;
	return *this;
}



///-------------------------------------------------------------------------------------------------
// End of vector_2d.cpp
///-------------------------------------------------------------------------------------------------
