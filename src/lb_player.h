///-------------------------------------------------------------------------------------------------
// file:	lb_player.h
//
// summary:	Declares the player entity class
///-------------------------------------------------------------------------------------------------

#ifndef LBPLAYER_H
#define LBPLAYER_H


// Headers
#include "vector_2d.h"
#include <stdint.h>
#include "gba.h"
#include "lb_baseentity.h"
#include "gba_keys.h"
#include "lb_constants.h"
#include "lb_events.h"


class Player : public Entity
{
	public:
		Player(int32_t xPos, int32_t yPos);

		~Player();

		void update();

		void handleInput();
	
		void handleEntityCollision(Entity *e);
		
		void draw(const Vector2D &camOffset);
	
		void setupObj();
		
		void handleTileCollision(int32_t tileType, int32_t tileIndex, int32_t w, int32_t h);
		
	private:
		
		GBAKeys *_keysHandler;
		
		// Timer for the attack state
		int32_t _attackTimerCounter;
		const int32_t _attackTimerThresholdSword;
		const int32_t _attackTimerTreshold;
		
		// Timer for the hurt state
		int32_t _hurtTimerCounter;
		const int32_t _hurtTimerTreshold;
		
		// Timer for looping through moving animations
		int32_t _movAnimCounter;
		const int32_t _moveAnimTimerThreshold;
		
		// Current frame of animation
		int32_t _currAnimFrame;
		
		// Position of graphics with respect to centre of logic position of the sprite
		Vector2D _graphicsPos;
		
		// Whether the attack routine has started or not
		bool _attacked;
};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_baseentity.h
///-------------------------------------------------------------------------------------------------