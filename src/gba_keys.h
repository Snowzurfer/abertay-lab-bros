///-------------------------------------------------------------------------------------------------
// file:	gba_keys.cpp
//
// summary:	Singleton for accessing the GBA buttons everywhere with also
//			utlity functions
///-------------------------------------------------------------------------------------------------

#ifndef GBAKEYS_H
#define GBAKEYS_H

// Headers
#include "gba.h"
#include <stdint.h>

class GBAKeys
{
	public:
		static GBAKeys* Instance();
		
		// Check whether a certain button has been pressed
		bool isBPressed(uint16_t button);

		// Check whether a certain button has JUST been pressed
		bool isBJustPressed(uint16_t button);
		
		// Update current keys
		void updateCurrent();
		
		// Update previous keys
		void updatePrevious();
		
	protected:
		// Constructor
		GBAKeys();
		
	private:
		static GBAKeys *_instance;
		
		// Previous & current key states
		uint16_t _prevKeys;
		uint16_t _currKeys;
};

#endif

///-------------------------------------------------------------------------------------------------
// End of gba_keys.h
///-------------------------------------------------------------------------------------------------