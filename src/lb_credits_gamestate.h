///-------------------------------------------------------------------------------------------------
// file:	lb_credits_gamestate.h
//
// summary:	Represents the credits state
///-------------------------------------------------------------------------------------------------

#ifndef CREDITS_STATE_H
#define CREDITS_STATE_H

// Headers
#include <stdint.h>
#include <stdlib.h>
#include "gba.h"
#include "font.h"
#include "gba_exp.h"
#include "rect.h"
#include "gba_keys.h"
#include "lb_events.h"
#include "lb_constants.h"
#include "base_state.h"



class Credits : public BaseState
{
	public:

		// Defaults constructor
		Credits(void);

		// Destructor
		~Credits(void);
		
		// Handle events such as input
		void handleEvents();

		// Update internals of the state
		void update();

		// Render internals
		void render();

		// Load eventual media
		bool loadMedia();

	private:
		
		// String buffer for printing text
		char *_stringBuffer;
		
		// Reference to key handler
		GBAKeys *_keysHandler;
};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_credits_gamestate.h
///-------------------------------------------------------------------------------------------------

