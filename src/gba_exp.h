///-------------------------------------------------------------------------------------------------
// file:	gba_exp.h
//
// summary:	Contains utility functions
///-------------------------------------------------------------------------------------------------


#ifndef GBAEXP_H
#define GBAEXP_H

// Headers
#include "gba.h"
#include <stdint.h>
#include "font.h"
#include <cstdio>

// Additional masks for GBA registers
#define ATTR2_ID8_MASK ATTR2_ID(0x3FF)
#define ATTR0_OM_MASK (0x300)

// Check whether a certain button has been pressed
bool isBPressed(uint16_t button, uint16_t reg = REG_KEYINPUT);

// Check whether a certain button has JUST been pressed
bool isBJustPressed(uint16_t button, uint16_t prevReg, uint16_t reg = REG_KEYINPUT);

/*
 * Draw a string at (x, y) on ScreenBlock sBlock
 */
void drawText(int32_t x, int32_t y, const char string[], int32_t sBlock);

// Clear with tile "tile" a given 32x32 SB
void clearScreenBlock32(int32_t sBlock, int32_t tile);


// Swap two elements of an array
static inline void swapPos(int32_t *p, int32_t posA, int32_t posB) {
	int32_t temp = *(p + posA);
	*(p + posA) = *(p + posB);
	*(p + posB) = temp;
}

// Returns the sign of an integer
int32_t sigNum(int32_t x);

/*
 * Print an integer at (x, y) on ScreenBlock sBlock
 */
void drawText(int32_t x, int32_t y, int32_t integer, int32_t sBlock);

// Set an object's tile index (most useful for animation)
static inline void setObjectTileIndex(int object, int index) {
	ObjAttr &obj(ObjBuffer[object]);
	obj.attr2 = ((obj.attr2 & ~ATTR2_ID8_MASK) | ATTR2_ID8(index));
}

// Flip an object with respect of the X axis depending on the value of (flip)
static inline void setObjectFlipX(int object, bool flip) {
	ObjAttr& obj(ObjBuffer[object]);
	if(flip == 0)
		obj.attr1 &= ~(ATTR1_VFLIP);
	else
		obj.attr1 |= ATTR1_VFLIP;
}

// Flip an object with respect of the Y axis depending on the value of (flip)
static inline void setObjectFlipY(int object, bool flip) {
	ObjAttr& obj(ObjBuffer[object]);
	if(flip == 0)
		obj.attr1 &= ~(ATTR1_HFLIP);
	else
		obj.attr1 |= ATTR1_HFLIP;
}

// Hide or unhide a given sprite
static inline void setObjectVisibility(int object, bool visible) {
	ObjAttr& obj(ObjBuffer[object]);
	if(visible == 0) {
		obj.attr0 = ((obj.attr0 & ~ATTR0_OM_MASK) | ATTR0_HIDE);
	}
	else
		obj.attr0 = ((obj.attr0 & ~ATTR0_OM_MASK) | ATTR0_REG);
}

#endif

///-------------------------------------------------------------------------------------------------
// End of gba_exp.h
///-------------------------------------------------------------------------------------------------