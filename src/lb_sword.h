// Sword entity

///-------------------------------------------------------------------------------------------------
// file:	lb_player.h
//
// summary:	Declares the player entity class
///-------------------------------------------------------------------------------------------------

#ifndef LBSWORD_H
#define LBSWORD_H


// Headers
#include <stdint.h>
#include "gba.h"
#include "lb_baseentity.h"

class Sword : public Entity
{
	public:
		Sword(int32_t xPos, int32_t yPos, FacingDir dir);

		~Sword();

		void draw(const Vector2D &camOffset);
		
	private:
	
		// Position of graphics with respect to centre of logic position of the sprite
		Vector2D _graphicsPos;
		
		// Current frame of animation
		int32_t _currAnimFrame;
};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_baseentity.h
///-------------------------------------------------------------------------------------------------