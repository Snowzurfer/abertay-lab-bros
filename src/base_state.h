///-------------------------------------------------------------------------------------------------
// file:	base_state.h
//
// summary:	Class representing the basic state from which every state class inherits
//			virtual functions.
///-------------------------------------------------------------------------------------------------


#ifndef BASESTATE_H
#define BASESTATE_H

// Headers
#include <stdint.h>
#include "lb_events_gamestates.h"

// Different types of game states
enum gameStates
{
	STATE_NULL = 0,
	STATE_INTRO,
	STATE_TITLE,
	STATE_INGAME,
	STATE_GAMEOVER,
	STATE_MENU,
	STATE_EXIT,
	STATE_CREDITS,
	STATE_SETTINGS
};

// Class definition
class BaseState
{
	public:

		// Contructor
		BaseState(void);

		// Destructor
		virtual ~BaseState(void) {};

		// Handle SDL events
		virtual void handleEvents() = 0;

		// Update game logic
		virtual void update() = 0;

		// Render the current state
		virtual void render() = 0;

		// Load assets
		virtual bool loadMedia() = 0;

		// Dirty and quick way of telling if the program has been closed;
		// Will be replaced by events dispatched by the states to inform
		// the manager and consequently the main
		bool quit;

	protected:

		// The ID of the next state
		int32_t nextStateID;
		
		// Reference to events handler (singleton pattern; ugly, but quick way to make it work)
		EventsGameStates *eH;
};

#endif

///-------------------------------------------------------------------------------------------------
// End of base_state.h
///-------------------------------------------------------------------------------------------------