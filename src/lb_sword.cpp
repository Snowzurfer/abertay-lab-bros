

// Headers
#include "lb_sword.h"

Sword::Sword(int32_t xPos, int32_t yPos, FacingDir dir): 
				Entity(xPos, yPos, 1, ENT_SWORD),
				_graphicsPos(Vector2D(xPos - 8, yPos - 8)){
	
	_velocity = Vector2D(0, 0);
	
	// Set frame and bounding box depending on direct
	switch(dir) {
		case DIR_NORTH: {
			_currAnimFrame = Const::FRM_SWORD_N;
			setObjectFlipX(_objNum, true);
			delete _boundingBox;
			_boundingBox = new Rect(xPos - 2, yPos - 1, 4, 9);
			break;
		}
		case DIR_EAST: {
			_currAnimFrame = Const::FRM_SWORD_E;
			delete _boundingBox;
			_boundingBox = new Rect(xPos - 8, yPos - 2, 9, 4);
			setObjectFlipY(_objNum, false);
			break;
		}
		case DIR_WEST: {
			_currAnimFrame = Const::FRM_SWORD_W;
			setObjectFlipY(_objNum, true);
			_boundingBox = new Rect(xPos - 1, yPos - 2, 9, 4);
			break;
		}
		case DIR_SOUTH: {
			_currAnimFrame = Const::FRM_SWORD_S;
			setObjectFlipX(_objNum, false);
			_boundingBox = new Rect(xPos - 2, yPos - 8, 4, 9);
			break;
		}
	}
}

void Sword::draw(const Vector2D &camOffset) {
	_graphicsPos.x = (_pos.x - 8) - camOffset.x;
	_graphicsPos.y = (_pos.y - 8) - camOffset.y;
	
	// The entity is outside the boundaries of the screen
	if(_graphicsPos.x >= Const::RENDERAREA_W ||
		_graphicsPos.x <= 0 || 
		_graphicsPos.y >= Const::RENDERAREA_H ||
		_graphicsPos.y <= 0) {
		// Hide it
		if(_visible) {
			setObjectVisibility(_objNum, false);
			_visible = false;
		}
		setObjectVisibility(_objNum, false);
	}
	else {
		if(!_visible) {
			setObjectVisibility(_objNum, true);
			_visible = true;
		}
		
		// Set sprite's position relative to the camera
		SetObjectX(_objNum, _graphicsPos.x);
		SetObjectY(_objNum, _graphicsPos.y);
		
		// Set the correct frame (eventual flipping happens in update)
		setObjectTileIndex(_objNum, _currAnimFrame);
	}
	
}

Sword::~Sword() {
	//delete _spriteTileNum;
	//_spriteTileNum = NULL;
	//delete _anim;
	//--objNumCounter;
}




