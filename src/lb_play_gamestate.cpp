///-------------------------------------------------------------------------------------------------
// file:	play.cpp
//
// summary:	Implements the ingame state class
///-------------------------------------------------------------------------------------------------

#include "lb_play_gamestate.h"


PlayState::PlayState(void) {
	dunSeed = 0;
	dunDiff = 1;
	
	stringBuffer = new char[100];
	
	eH = Events::Instance();
	
	eHStates = EventsGameStates::Instance();
	
	player = new Player(0, 0);
	
	dunManager = new Dungeon();
	
	// Create the map and return the pool of enemies plus the player with
	// his position set
	ePool = dunManager->makeMap(dunDiff, 32, player);
	
	cam = new Camera(0,0, Const::RENDERAREA_W, Const::RENDERAREA_H, player);
}

// Manage entity-tile collisions
void PlayState::_checkAndResolveCollisionsMap(Entity *e, int32_t *dunMap) {

	// Create the array containing the tiles surrounding the position
	int32_t *surroundingTiles = new int32_t[9];
	
	// Get position in tiles of the argument
	int32_t tPosX = ((e->getPos().x) / Const::TILE_WIDTH);
	int32_t	tPosY = ((e->getPos().y) / Const::TILE_HEIGHT);
	int32_t col = 0;
	int32_t row = 0;
	
	// For the 9 tiles we have to get
	for(int32_t i = 0; i < 9; i++) {
		// Calculate current row and column
		col = i % 3;
		row = i / 3;
		
		// Retrieve tile ID and store it
		*(surroundingTiles + i) = ((tPosX - 1 + col) + (MAP_WIDTH_T * (tPosY - 1 + row)));
	}
	
	// Sort the tiles differently
	swapPos(surroundingTiles, 0, 7);
	swapPos(surroundingTiles, 2, 3);
	swapPos(surroundingTiles, 3, 5);
	swapPos(surroundingTiles, 4, 7);
	swapPos(surroundingTiles, 7, 8);
	
	// Loop through the 9 surrounding tiles
	for(int32_t i = 0; i < 9; i++) {
				
		// If the current tile is actually a solid tile
		if(dunMap[*(surroundingTiles + i)] != Const::TILE_FLOOR) {
			// Get entity's bounding box. Since its position might change between one collision
			// check and the other, it is necessary to retrieve it every loop
			Rect *eBB = e->getBoundingBox();
			
			// Calculate world position of the tile
			Rect tileR((*(surroundingTiles + i) % MAP_WIDTH_T) * Const::TILE_WIDTH,
					(*(surroundingTiles + i) / MAP_WIDTH_T) * Const::TILE_HEIGHT,
					Const::TILE_WIDTH,
					Const::TILE_HEIGHT);

			// If the tile and the entity collide
			if(Rect::checkCollision(*eBB, tileR)) {

				// Get intersection rectangle
				int32_t x = fmax(eBB->pos.x, tileR.pos.x);
				int32_t y = fmax(eBB->pos.y, tileR.pos.y);
				int32_t w = fmin(eBB->pos.x + eBB->w, tileR.pos.x + tileR.w);
				int32_t h = fmin(eBB->pos.y + eBB->h, tileR.pos.y + tileR.h);
				w-=x;
				h-=y;
				
				// Inform the entity
				e->handleTileCollision(dunMap[*(surroundingTiles + i)], i, w, h);
				
			}
		}
	}
	
	// Free memory
	delete [] surroundingTiles;
	
}

// After having managed collisions with the map, manage collisions entity-entity
void PlayState::_checkAndResolveCollisionsEntities(Entity* e, std::vector<Entity*> *ePool, std::vector<ProcessedEntColl> &procEntPool) {
	// Check if the current entity is colliding with any other entity
	for(std::vector<Entity*>::iterator ePoolItor = ePool->begin(); ePoolItor != ePool->end(); ePoolItor++) {
		// Check that the entity processed is not the one which we want to resolve collisions with
		if((*ePoolItor) != e) {
		
			// Get unique IDs of the entities
			int32_t eBObjNum = (*ePoolItor)->getObjNum();
			int32_t eAObjNum = e->getObjNum();
			
			// Loop through the already processed entities pool
			for(std::vector<ProcessedEntColl>::iterator procEntPoolItor = procEntPool.begin(); procEntPoolItor != procEntPool.end(); procEntPoolItor++) {
				// If they have already been processed this frame
				if((eAObjNum == procEntPoolItor->eAID && eBObjNum == procEntPoolItor->eBID) ||
					(eBObjNum == procEntPoolItor->eAID && eAObjNum == procEntPoolItor->eBID)) {
					// Just skip the function call if the entities have already been processed
					return;
				}
			}
			
			// Get entity's bounding box. Since its position might change between one collision
			// check and the other, it is necessary to retrieve it every loop
			Rect *eABB = e->getBoundingBox();
				
			// Get entity's bounding box. Since its position might change between one collision
			// check and the other, it is necessary to retrieve it every loop
			Rect *eCBB = (*ePoolItor)->getBoundingBox();

			// If the two entities collide
			if(Rect::checkCollision(*eABB, *eCBB)) {
				
				// Tell entities of the collision between them and let them
				// react accordingly to their properties
				e->handleEntityCollision((*ePoolItor));
				(*ePoolItor)->handleEntityCollision(e);
				
				// Save that these two unique entities have resolved collisions
				// between each other, so that if B has stored a collision event
				// too, nothing will happen
				ProcessedEntColl pec = {eAObjNum, eBObjNum};
				procEntPool.push_back(pec);
			}
		}
	}
}


PlayState::~PlayState(void) {
	

	delete [] stringBuffer;
}

void PlayState::handleEvents() {
	// Handle user input
	player->handleInput();
}

// Update is called constantly
void PlayState::update() {
	
	// Increase seed counter
	dunSeed ++;
	
	// Update objects
	for(ePoolItor = ePool->begin(); ePoolItor != ePool->end(); ePoolItor++) {
		(*ePoolItor)->update();
		
		// If the entity is dead
		if((*ePoolItor)->getCurrState() == STATE_DEAD) {
			// In case it is the player, remove also its reference
			if ((*ePoolItor)->getType() == ENT_PLAYER) {
				
				/*var arrayToGameOver:Array = new Array();
				arrayToGameOver.push(_player.score);*/
				
				//player = NULL;
				
				Events *eH = Events::Instance();
			
				// Dispatch event for deleting the sword
				eH->dispatchEvent(Event(Const::EVENT_PLAYERDEAD));
				
				// Change state to gameover TODO
				//dispatchEvent(new LmStateEvent(LmStateEvent.CHANGE_STATE, Constants.STATE_GAMEOVER, arrayToGameOver));
			}
			// If it is an enemy, add its score to the player's one
			else if ((*ePoolItor)->getType() == ENT_ENEMY_A || (*ePoolItor)->getType() == ENT_ENEMY_B) {
				player->_score += (*ePoolItor)->getScore();
				
				delete (*ePoolItor);
				ePoolItor = ePool->erase(ePoolItor);
				--ePoolItor; // Correct itor's position
			}
			
			
			
			
			
			
			
		}
	}
	
	// Resolve collisions with map
	for(ePoolItor = ePool->begin(); ePoolItor != ePool->end(); ePoolItor++) {
		_checkAndResolveCollisionsMap((*ePoolItor), dunManager->dungeonMap);
	}
	
	// Resolve collisions with entities//
	// Inform entities of collisions with other entities so that they can
	// act accordingly
	std::vector<ProcessedEntColl> procEntPool;
	for(ePoolItor = ePool->begin(); ePoolItor != ePool->end(); ePoolItor++) {
		
		_checkAndResolveCollisionsEntities(*ePoolItor, ePool, procEntPool);
		
		// Set the new position for the entity
		(*ePoolItor)->setPos((*ePoolItor)->getDesPos());
	}
	

	
	// Clear the pool (at end of game loop should clear itself by going out
	// of scope but let's just make sure :) )
	procEntPool.clear();
	
	// Update camera
	cam->update();
	
	
	
	// Loop through the game events and act accordingly
	Event e = eH->pollEvent();
	while(e.eType != Const::EVENT_NULL) {
		switch(e.eType) {
			case Const::EVENT_CHANGELEVEL: {
			
				// Clear pool of entities pointers (minus the player)
				ClearObjects();
				for(ePoolItor = ePool->begin() + 1; ePoolItor != ePool->end(); ePoolItor++) {
					delete (*ePoolItor);
				}
				ePool->clear();
				delete ePool;
				
				// Player is still alive
				Entity::objNumCounter = 1;
				
				// Increase diff counter
				++ dunDiff;

				// Generate new map
				ePool = dunManager->makeMap(dunDiff, dunSeed, player);
				
				// Re-setup player's sprite after having used clearobjects
				player->setupObj();
				
				break;
			}
			case Const::EVENT_PLAYERDEAD: {
			
				// Dispatch change state event
				std::vector<int32_t> args;
				args.push_back(Const::STATE_GAMEOVER);
				args.push_back(player->getScore());
				args.push_back(dunDiff);
				eHStates->dispatchEvent(GameStateEvent(Const::EVENT_CHANGESTATE, args));
				
				
				// Clear pool of entities pointers
				ClearObjects();
				for(ePoolItor = ePool->begin(); ePoolItor != ePool->end(); ePoolItor++) {
					delete (*ePoolItor);
				}
				ePool->clear();
				delete ePool;
				
				Entity::objNumCounter = 0;
				
				break;
			}
			case Const::EVENT_SPAWNSWORD: {
				// Get positional data from the player
				FacingDir pDir = player->getCurrDir();
				int32_t xPos = player->getPos().x;
				int32_t yPos = player->getPos().y;
				

				// Move the sword so that it stays perfectly into
				// the player's hand
				switch(pDir) {
					case DIR_NORTH: {
						xPos -= 3;
						yPos = yPos - 16;
						break;
					}
					case DIR_EAST: {
						xPos = xPos + 16;
						yPos += 1;
						break;
					}
					case DIR_WEST: {
						xPos = xPos - 16;
						yPos += 1;
						break;
					}
					case DIR_SOUTH: {
						xPos += 3;
						yPos = yPos + 16;
						break;
					}
				}
				
				// Spawn the sword
				sword = new Sword(xPos, yPos, pDir);
				ePool->push_back(sword);
				break;
			}
			case Const::EVENT_DELETESWORD: {
				sword->setCurrState(STATE_DEAD);
				
				sword = NULL;
				break;
			}
		}
		
		// Retrieve the next event in the queue
		e = eH->pollEvent();
	}
}

void PlayState::render() {

	// Render map by refreshing the screenblock
	
	// Used for map refresh
	int32_t xPosTile = 0, yPosTile = 0;
		
	// Get the camera's box
	Rect *camBox = cam->getBox();
	
	
	// Get cam X pos in logic tiles
	int32_t camTileX = camBox->pos.x / Const::TILE_WIDTH;
	
	// Get cam Y pos in logic tiles
	int32_t camTileY = camBox->pos.y / Const::TILE_HEIGHT;
	
	// Get cam end X pos in logic tiles
	int32_t camTileWidth = ((camBox->pos.x + camBox->w) / Const::TILE_WIDTH) + 1;
	
	// Get cam end Y pos in logic tiles
	int32_t camTileHeight = ((camBox->pos.y + camBox->h) / Const::TILE_HEIGHT) + 1;


	// Draw entities (sprites)
	for(ePoolItor = ePool->begin(); ePoolItor != ePool->end(); ePoolItor++) {
		(*ePoolItor)->draw(camBox->pos);
	}
	
	
	// Update the OAM (sprites)
	UpdateObjects();
	
	
	// Scroll the background (tilemap)
	cam->updateScrolling();
	
	
	// Render the UI
	sprintf(stringBuffer, "SCORE:%d", player->getScore());
	drawText(2, 17, stringBuffer, Const::SB_TEXT);
	
	sprintf(stringBuffer, "HEALTH:%d", player->getHealth());
	drawText(2, 19, stringBuffer, Const::SB_TEXT);
	
	sprintf(stringBuffer, "LEVEL:%d", dunDiff);
	drawText(20, 17, stringBuffer, Const::SB_TEXT);
	
	
	// Update the screenblock by refreshing it
	// For each tile row from start to end
	for(int32_t y = camTileY; y < camTileHeight; y ++) {
		// For each tile column in that row
		for(int32_t x = camTileX; x < camTileWidth; x ++) {
			// Set the corresponding tile in the SB
			
			// Get position in the array of tiles (LOGIC TILES)
			int32_t pos = x + (y * MAP_WIDTH_T);
			
			// Get position in tiles (GBA TILES)
			xPosTile = x << 1;
			yPosTile = y << 1;
			
			// Make it relative to camera offset
			xPosTile -= cam->getBoxTiles()->pos.x;
			yPosTile -= cam->getBoxTiles()->pos.y;
			
			// Apply current tile to respective tile position on the screenblock
			// Uses pointer arithmetic for faster indexing
			for(int32_t t = 0; t < 4; t++) {
				SetTile(Const::SB_BACKGROUND, xPosTile+((t+2)%2), yPosTile+(t >> 1), *(*(Const::tileTypes + *(dunManager->dungeonMap + pos)) + t));
			}
		}
	}
}

// Load assets
bool PlayState::loadMedia() {

	// Print black stripe at bottom of screen
	for(int32_t r = 0; r < 4; r ++) {
		for(int32_t c = 0; c < Const::RENDERAREA_W / 8; c ++) {
			SetTile(Const::SB_UI, c, r + (Const::RENDERAREA_H / 8), 1);
		}
	}

	return true;
}