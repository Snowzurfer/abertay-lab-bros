///-------------------------------------------------------------------------------------------------
// file:	dugeon.cpp
//
// summary:	Implements the dungeon class
///-------------------------------------------------------------------------------------------------

#include "lb_dungeon.h"

Dungeon::Dungeon() {
	maxRooms = 100;
	dungeonMap = new int32_t[MAP_HEIGHT_T * MAP_WIDTH_T];
}

Dungeon::~Dungeon() {
	delete []dungeonMap;
}

std::vector<Entity*> *Dungeon::makeMap(int32_t diffLevel, int32_t seed, Player *player) {
	
	delete []dungeonMap;
		
	dungeonMap = new int32_t[MAP_HEIGHT_T * MAP_WIDTH_T];
	
	// Create the pool of entities
	std::vector<Entity*> *entitiesPool = new std::vector<Entity*>();
	
	// Seed the rand function
	srand(seed);

	// First reset the array with the tiles
	// to only empty tiles
	for(int32_t y = 0; y < MAP_HEIGHT_T; y++) {
		for(int32_t x = 0; x < MAP_WIDTH_T; x++) {
			dungeonMap[x + (y * MAP_WIDTH_T)] = Const::TILE_NULL;
		}
	}

	// Vector to hold all the rooms which will be created
	std::vector<Room*> rooms;
	std::vector<Room*>::iterator itor;

	// Create random rooms
	for(int32_t i = 0; i < maxRooms; i++) {
		// Create coordinates and size of a room
		int32_t w = getRand(minRoomSize, maxRoomSize);
		int32_t h = getRand(minRoomSize, maxRoomSize);
		int32_t x = getRand(1, MAP_WIDTH_T - w - 1);
		int32_t y = getRand(1, MAP_HEIGHT_T - h - 1);


		// Create a room with randomised values
		Room *newRoom = new Room(x, y, w, h);

		// Check if it intersects with other rooms
		bool failed = false;
		for(itor = rooms.begin(); itor != rooms.end(); itor++) {
			if(newRoom->intersects(*(*itor))) {
				failed = true;
				break; // Stop loop
			}
		}

		// If the room is ok
		if(!failed) {
			// Dig a new room
			digRoom(newRoom);

	
			// If there is at least one room
			if(rooms.size() != 0) {
				// Store centre of previous and new room
				Vector2D prevCentre = rooms[rooms.size() - 1]->getCentrePos();
				Vector2D newCentre = newRoom->getCentrePos();

				// Dig corridors based on centers, and start
				// randomly with a vertical or horizontal one
				if(rand()% 2) {
					digHCorridor(prevCentre.x, newCentre.x, prevCentre.y);
					digVCorridor(prevCentre.y, newCentre.y, newCentre.x);
				}
				else {
					digVCorridor(prevCentre.y, newCentre.y, prevCentre.x);
					digHCorridor(prevCentre.x, newCentre.x, newCentre.y);
				}

			}
			rooms.push_back(newRoom);
		}
	}
	
	
	
	// Refine the dungeon's graphics by placing wall tiles
	for(int32_t y = 0; y < MAP_HEIGHT_T; y++) {
		for(int32_t x = 0; x < MAP_WIDTH_T; x++) {
			// If the current tile is a null tile
			if(dungeonMap[x + (y * MAP_WIDTH_T)] == Const::TILE_NULL) {
				// Get the indexes of the tiles surrounding it
				int32_t *surroundingTiles = getSurroundingTilesIndexes(Vector2D((x*Const::TILE_WIDTH) + 1, (y*Const::TILE_HEIGHT) + 1));
				
				// If it has a floor tile beneath it
				if(*(dungeonMap + *(surroundingTiles + 7)) == Const::TILE_FLOOR) {
					// Set the current tile to be a wall with baseboard
					dungeonMap[x + (y * MAP_WIDTH_T)] = Const::TILE_BOTTOM;
					
				}
				// Else check for surrounding tiles
				else {
					for(int32_t i = 0; i < 9; i++) {
						// If at least one of the surrounding tiles is a floor tile
						if(*(dungeonMap + *(surroundingTiles + i)) == Const::TILE_FLOOR) {
							// Stop loop and set it to be a wall
							dungeonMap[x + (y * MAP_WIDTH_T)] = Const::TILE_CENTRE;
							break;
						}
					}
				}
				
				delete[] surroundingTiles;
			}
		}
	}
	
	
	// Set player's position always in first room in list; can be improved by randomising
	// the room, but this way debugging is easier
	itor = rooms.begin();
	player->setPos(Vector2D((((*itor)->getBoxTiles().pos.x * Const::TILE_WIDTH) + Const::SPRITE_SIZE + 2), (((*itor)->getBoxTiles().pos.y * Const::TILE_HEIGHT) + Const::SPRITE_SIZE + 2)));
	
	
	entitiesPool->push_back(player);
	
	// Get the number of half elements in the rooms array
	int32_t roomsArrayHalfLenght = (rooms.size() / 2);
	
	// Loop for half the length of the array
	for(int32_t i = 0; i < ENT_NUM - 1; i++) {
		// Get a random room where to spawn the elements
		itor = rooms.begin() + (rand() % rooms.size());
		
		int32_t xPos = ((*itor)->getBoxTiles().pos.x * Const::TILE_WIDTH) + (rand() % ((*itor)->getBoxTiles().w * Const::TILE_WIDTH)) -20;
		int32_t yPos = ((*itor)->getBoxTiles().pos.y * Const::TILE_HEIGHT) + (rand() % ((*itor)->getBoxTiles().h * Const::TILE_HEIGHT)) - 20;
		
		if(rand()%2) {
			entitiesPool->push_back(new EnemyA(xPos + Const::SPRITE_SIZE, yPos + 8, diffLevel));
		}
		else {
			entitiesPool->push_back(new EnemyB(xPos + Const::SPRITE_SIZE, yPos + 8, diffLevel));
		}
		
	}
	
	// Get a random room where to spawn the stairs
	itor = rooms.begin() + (rand() % rooms.size());
	
	// Select a random tile where to spawn the stairs (use the room found
	// and multiply width times height to get the max number of tiles)
	int32_t stairsTile = rand() % ((*itor)->getBoxTiles().w * (*itor)->getBoxTiles().h);
	
	setSingleTileInRoom(*itor, (stairsTile % (*itor)->getBoxTiles().w), (stairsTile / (*itor)->getBoxTiles().h), Const::TILE_STAIRS);
	
	// Free memory
	for(itor = rooms.begin(); itor != rooms.end(); itor++) {
		delete *itor;
    }
	rooms.clear();
	
	return entitiesPool;
}

void Dungeon::digRoom(const Room *r) {
	int32_t posY = r->getBoxTiles().pos.y;
	int32_t posX = r->getBoxTiles().pos.x;
	int32_t endX = posX + r->getBoxTiles().w;
	int32_t endY = posY + r->getBoxTiles().h;

	for(int32_t y = posY; y < endY; y++) {
		for(int32_t x = posX; x < endX; x++) {
			dungeonMap[x + (y * MAP_WIDTH_T)] = Const::TILE_FLOOR;
		}
	}
}

void Dungeon::digVCorridor(int32_t y1, int32_t y2, int32_t x) {
	for(int32_t y = std::min(y1, y2); y < std::max(y1,y2) + 1; y++) {
		dungeonMap[x + (y * MAP_WIDTH_T)] = Const::TILE_FLOOR;
	}
}

void Dungeon::digHCorridor(int32_t x1, int32_t x2, int32_t y) {
	for(int32_t x = std::min(x1, x2); x < std::max(x1, x2) + 1; x++) {
		dungeonMap[x + (y * MAP_WIDTH_T)] = Const::TILE_FLOOR;
	}
}

int32_t Dungeon::getRand(int32_t min, int32_t max) {
	/**
		*  Save the random generated number, and use it to seed the next time.
		*  This expedient is used since there's no time lib on the GBA for
		*  seeding the rand function the classic way.
		*/
	seedIncrement = rand() % 100 + 1;
	
	// Seed the rand function
	//srand(seedIncrement);
	
	// Generate number for capping
	int32_t n = max - min + 1;

	// Get random number in range
	int32_t i = rand() % n;
 
	// In case the generated number is negative
	if(i < 0) {
		i = -i;
	}
 
	return min + i;
}


void Dungeon::setSingleTileInRoom(const Room *r, int32_t tXPos, int32_t tYPos, int32_t tType) {
	int32_t tilePos = (r->getBoxTiles().pos.x) + 
					(r->getBoxTiles().pos.y * MAP_WIDTH_T) +
					(tXPos) +
					(tYPos * MAP_WIDTH_T);
	
	dungeonMap[tilePos] = tType;
}

Vector2D Dungeon::getTileCoordFromPixel(const Vector2D &pV) {
	return Vector2D(pV.x / Const::TILE_WIDTH, pV.y /Const::TILE_HEIGHT);
}

int32_t *Dungeon::getSurroundingTilesIndexes(const Vector2D &pV) {
	// Create the array containing the tiles surrounding the position
	int32_t *array = new int32_t[9];
	
	// Get position in tiles of the argument
	int32_t tPosX = ((pV.x + 8) / Const::TILE_WIDTH);
	int32_t	tPosY = ((pV.y + 8) / Const::TILE_HEIGHT);
	int32_t col = 0;
	int32_t row = 0;
	
	// For the 9 tiles we have to get
	for(int32_t i = 0; i < 9; i++) {
		// Calculate current row and column
		col = i % 3;
		row = i / 3;
		
		// Retrieve tile ID and store it
		*(array + i) = ((tPosX - 1 + col) + (MAP_WIDTH_T * (tPosY - 1 + row)));
	}
	
	return array;
}

///-------------------------------------------------------------------------------------------------
// End of lb_dugeon.cpp
///-------------------------------------------------------------------------------------------------
