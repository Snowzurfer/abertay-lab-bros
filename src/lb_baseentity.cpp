///-------------------------------------------------------------------------------------------------
// file:	lb_baseentity.cpp
//
// summary:	Source file for the BaseEntity class
///-------------------------------------------------------------------------------------------------


// Headers
#include "lb_baseentity.h"

// Initialise the static counter
int32_t Entity::objNumCounter = 0;


Entity::Entity(int32_t xPos, int32_t yPos, int32_t vel, EntityType eType): 
				_maxVel(vel), _objNum(objNumCounter), _type(eType) {
	_pos = Vector2D(xPos, yPos);
	
	_velocity = Vector2D(0, 0);
	
	_health = 3;
	
	_currDir = DIR_EAST;
	_prevDir = _currDir;
	
	_currState = STATE_IDLE;
	_prevState = _currState;
	
	_boundingBox = new Rect(xPos - 6, yPos - 7, 12, 15);
	
	
	_moveRight = false;
	_moveLeft = false;
	_moveUp = false;
	_moveDown = false;
	_attack = false;
	
	
	_alive = true;
	
	_visible = true;

	_collMap = false;
	
	_setupObj();
	
	_score = 0;
}

Entity::~Entity() {
	delete _boundingBox;
	setObjectVisibility(_objNum, false);
}

void Entity::update() {
	// Behave depending on current state
	switch(_currState){
		case STATE_IDLE: {
			// If the entity is not moving horizontally
			if(!_moveLeft && !_moveRight) {
				_velocity.x = 0;
			}
				
			// If the entity is not moving vertically
			if(!_moveUp && !_moveDown) {
				_velocity.y = 0;
			}
			
			// Stop entity if it doesn't want to move
			if(!_moveLeft && !_moveRight && !_moveUp && !_moveDown) {
			
				// Set state
				_prevState = _currState;
				_currState = STATE_IDLE;
			}
			break;
		}
		case STATE_MOVING: {
			// If the entity is not moving horizontally
			if(!_moveLeft && !_moveRight) {
				_velocity.x = 0;
			}
				
			// If the entity is not moving vertically
			if(!_moveUp && !_moveDown) {
				_velocity.y = 0;
			}
			
			// Stop entity if it doesn't want to move
			if(!_moveLeft && !_moveRight && !_moveUp && !_moveDown) {
			
				// Set state
				_prevState = _currState;
				_currState = STATE_IDLE;
			}
			break;
		}
		case STATE_HURT: {
			// If the entity is not moving horizontally
			if(!_moveLeft && !_moveRight) {
				_velocity.x = 0;
			}
				
			// If the entity is not moving vertically
			if(!_moveUp && !_moveDown) {
				_velocity.y = 0;
			}
			
			// Stop entity if it doesn't want to move
			if(!_moveLeft && !_moveRight && !_moveUp && !_moveDown) {
			
				// Set state
				/*_prevState = _currState;
				_currState = STATE_IDLE;*/
			}
			break;
		}
		
	}
	

	
	// Update desired position based on velocity
	_desPos.x = _pos.x + _velocity.x;
	_desPos.y = _pos.y + _velocity.y;
	
	// Check if the entity has died
	if(_health <= 0) {
		_alive = false;
		_prevState = _currState;
		_currState = STATE_DEAD;
	}
}

void Entity::draw(const Vector2D &camOffset) {
	// Do nothing by default
}

void Entity::_setupObj() {
	// as soon as the main loop starts everything is
	// set correctly.
	SetObject(_objNum,
	          ATTR0_SHAPE(0) | ATTR0_8BPP | ATTR0_REG | ATTR0_Y(_pos.y),
			  ATTR1_SIZE(1) | ATTR1_X(_pos.x),
			  ATTR2_ID8(0));
}

Rect *Entity::getBoundingBox(void) {
	_boundingBox->pos.x = _desPos.x - 6;
	_boundingBox->pos.y = _desPos.y - 7;
	
	return _boundingBox;
}

void Entity::reduceHealth(int32_t amount) {
	_health -= amount;
}

void Entity::handleInput() {
	// Do nothing by default
}

void Entity::handleEntityCollision(Entity *e) {
	// Do nothing by default
}

void Entity::_stopMoving() {
	_moveLeft = _moveRight = _moveUp = _moveDown = false;
}

void Entity::setOneDir(FacingDir dir) {
	switch(dir) {
		case DIR_NORTH: {
			_moveLeft = _moveRight =  _moveDown = false;
			_moveUp = true;
			_prevDir = _currDir;
			_currDir = DIR_NORTH;
			
			break;
		}
		case DIR_EAST: {
			_moveLeft = _moveUp =  _moveDown = false;
			_moveRight = true;
			_prevDir = _currDir;
			_currDir = DIR_EAST;
			
			break;
		}
		case DIR_WEST: {
			_moveRight = _moveUp =  _moveDown = false;
			_moveLeft = true;
			_prevDir = _currDir;
			_currDir = DIR_WEST;
			
			break;
		}
		case DIR_SOUTH: {
			_moveRight = _moveUp =  _moveLeft = false;
			_moveDown = true;
			_prevDir = _currDir;
			_currDir = DIR_SOUTH;
			
			break;
		}
	}
}

void Entity::handleTileCollision(int32_t tileType, int32_t tileIndex, int32_t w, int32_t h) {
	// Do nothing by default
}

///-------------------------------------------------------------------------------------------------
// End of lb_baseentity.cpp
///-------------------------------------------------------------------------------------------------