///-------------------------------------------------------------------------------------------------
// file:	lb_enemyA.h
//
// summary:	Declares the enemyA entity class
///-------------------------------------------------------------------------------------------------

#ifndef LBENEMYA_H
#define LBENEMYA_H


// Headers
#include "vector_2d.h"
#include <stdint.h>
#include "gba.h"
#include "lb_baseentity.h"
#include "gba_keys.h"
#include "lb_constants.h"



class EnemyA : public Entity
{
	public:
		EnemyA(int32_t xPos, int32_t yPos, int32_t health);

		~EnemyA();

		void update();
		
		void handleEntityCollision(Entity *e);
		
		void draw(const Vector2D &camOffset);
		
		void handleTileCollision(int32_t tileType, int32_t tileIndex, int32_t w, int32_t h);
		
	private:
		
		// Timer for the hurt state
		int32_t _hurtTimerCounter;
		const int32_t _hurtTimerTreshold;
		
		// Timer for looping through moving animations
		int32_t _movAnimCounter;
		const int32_t _moveAnimTimerThreshold;
		
		// Current frame of animation
		int32_t _currAnimFrame;
		
		// Position of graphics with respect to centre of logic position of the sprite
		Vector2D _graphicsPos;
		
		// Invert current direction
		void _changeDir();
};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_baseentity.h
///-------------------------------------------------------------------------------------------------