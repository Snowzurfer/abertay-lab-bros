
//{{BLOCK(tiles_dungeon)

//======================================================================
//
//	tiles_dungeon, 64x32@8, 
//	+ palette 16 entries, not compressed
//	+ 32 tiles not compressed
//	Total size: 32 + 2048 = 2080
//
//	Time-stamp: 2014-05-21, 00:43:01
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.6
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_TILES_DUNGEON_H
#define GRIT_TILES_DUNGEON_H

#define tiles_dungeonTilesLen 2048
extern const unsigned short tiles_dungeonTiles[1024];

#define tiles_dungeonPalLen 32
extern const unsigned short tiles_dungeonPal[16];

#endif // GRIT_TILES_DUNGEON_H

//}}BLOCK(tiles_dungeon)
