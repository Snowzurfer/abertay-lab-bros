///-------------------------------------------------------------------------------------------------
// file:	base_state.cpp
//
// summary:	Constants need all around the code. They are wrapped into a namespace
//			for not polluting the global namespace.
///-------------------------------------------------------------------------------------------------


#ifndef LBCONSTANTS_H
#define LBCONSTANTS_H

// Headers
#include <stdint.h>

// Wrap consts in a namespace for not polluting the global namsepace
namespace Const {

	// Events constants
	const int32_t EVENT_NULL = -1;
	const int32_t EVENT_CHANGELEVEL = 0;
	const int32_t EVENT_PLAYERDEAD = 1;
	const int32_t EVENT_SPAWNSWORD = 2;
	const int32_t EVENT_DELETESWORD = 3;
	const int32_t EVENT_CHANGESTATE = 4;
	const int32_t EVENT_QUITGAME = 5;
	
	
	// Game states constants
	const int32_t MENU_SEL_START = 0;
	const int32_t MENU_SEL_CREDITS = 1;
	
	// Different types of game states
	enum gameStates
	{
		STATE_NULL = 0,
		STATE_INTRO,
		STATE_TITLE,
		STATE_INGAME,
		STATE_GAMEOVER,
		STATE_MENU,
		STATE_EXIT,
		STATE_CREDITS,
		STATE_SETTINGS
	};
	
	// Size of an entity sprite
	const int32_t SPRITE_SIZE = 16;

	// Entities frames for animation
	const int32_t FRM_PLAYER_N_WALK0 = 14; // Player N W0
	const int32_t FRM_PLAYER_N_WALK1 = 12; // Player N W1
	const int32_t FRM_PLAYER_E_WALK0 = 2; // Player E W0
	const int32_t FRM_PLAYER_E_WALK1 = 0; // Player E W1
	const int32_t FRM_PLAYER_W_WALK0 = 2; // Player W W0
	const int32_t FRM_PLAYER_W_WALK1 = 0; // Player W W1
	const int32_t FRM_PLAYER_S_WALK0 = 6; // Player S W0
	const int32_t FRM_PLAYER_S_WALK1 = 8; // Player S W1
	const int32_t FRM_PLAYER_N_IDLE = 10; // Player N I
	const int32_t FRM_PLAYER_S_IDLE = 4; // Player S I
	const int32_t FRM_PLAYER_W_IDLE = 0; // Player W I
	const int32_t FRM_PLAYER_E_IDLE = 0; // Player E I
	const int32_t FRM_PLAYER_N_ATK = 38; // Player N ATK
	const int32_t FRM_PLAYER_S_ATK = 36; // Player S ATK
	const int32_t FRM_PLAYER_E_ATK = 34; // Player E ATK
	const int32_t FRM_PLAYER_W_ATK = 34; // Player W ATK
	
	const int32_t FRM_ENEMYA_N_WALK0 = 64; // EnemyA N W0
	const int32_t FRM_ENEMYA_N_WALK1 = 66; // EnemyA N W1
	const int32_t FRM_ENEMYA_E_WALK0 = 40; // EnemyA E W0
	const int32_t FRM_ENEMYA_E_WALK1 = 42; // EnemyA E W1
	const int32_t FRM_ENEMYA_W_WALK0 = 40; // EnemyA W W0
	const int32_t FRM_ENEMYA_W_WALK1 = 42; // EnemyA W W1
	const int32_t FRM_ENEMYA_S_WALK0 = 44; // EnemyA S W0
	const int32_t FRM_ENEMYA_S_WALK1 = 46; // EnemyA S W1
	const int32_t FRM_ENEMYA_N_IDLE = 64; // EnemyA N I
	const int32_t FRM_ENEMYA_S_IDLE = 38; // EnemyA S I
	const int32_t FRM_ENEMYA_W_IDLE = 40; // EnemyA W I
	const int32_t FRM_ENEMYA_E_IDLE = 40; // EnemyA E I
	
	const int32_t FRM_ENEMYB_N_WALK0 = 96; // EnemyB N W0
	const int32_t FRM_ENEMYB_N_WALK1 = 98; // EnemyB N W1
	const int32_t FRM_ENEMYB_E_WALK0 = 70; // EnemyB E W0
	const int32_t FRM_ENEMYB_E_WALK1 = 72; // EnemyB E W1
	const int32_t FRM_ENEMYB_W_WALK0 = 70; // EnemyB W W0
	const int32_t FRM_ENEMYB_W_WALK1 = 72; // EnemyB W W1
	const int32_t FRM_ENEMYB_S_WALK0 = 74; // EnemyB S W0
	const int32_t FRM_ENEMYB_S_WALK1 = 76; // EnemyB S W1
	const int32_t FRM_ENEMYB_N_IDLE = 96; // EnemyB N I
	const int32_t FRM_ENEMYB_S_IDLE = 74; // EnemyB S I
	const int32_t FRM_ENEMYB_W_IDLE = 70; // EnemyB W I
	const int32_t FRM_ENEMYB_E_IDLE = 74; // EnemyB E I

	const int32_t FRM_SWORD_S = 100; // Sword S
	const int32_t FRM_SWORD_N = 100; // Sword N
	const int32_t FRM_SWORD_E = 78; // Sword E
	const int32_t FRM_SWORD_W = 78; // Sword E
	
	
	
	
	// Tiles consts
	
	// The width, in tiles (8x8) of the tileset
	const int32_t TILESET_WIDTH_TILES = 16;

	//Tile constants
	const int32_t TILE_WIDTH = 16;
	const int32_t TILE_HEIGHT = 16;
	const int32_t TILE_TYPES = 12;

	//The different type of tiles
	const int32_t TILE_NULL = 0;
	const int32_t TILE_BOTTOM = 1;
	const int32_t TILE_CENTRE = 2;
	const int32_t TILE_FLOOR = 3;
	const int32_t TILE_STAIRS = 4;


	// Array containing all the instances of logical tile types_
	extern const int32_t tileTypes[TILE_TYPES][4];
	
	
	
	
	// Screen consts
	const int32_t RENDERAREA_W = 240;
	const int32_t RENDERAREA_H = 128;
	
	const int32_t RENDERAREA_W_T = RENDERAREA_W / TILE_WIDTH;
	const int32_t RENDERAREA_H_T = RENDERAREA_H / TILE_HEIGHT;
	
	// Define the role of used screenblocks. For sanity sake
	enum screenBlocksRole {
		SB_BACKGROUND = 25,
		SB_TEXT = 27,
		SB_UI = 28
	};
	
	// Map constants
	const int32_t MAP_WIDTH_T = 25;
	const int32_t MAP_HEIGHT_T = 25;
	const int32_t MAP_WIDTH = MAP_WIDTH_T * TILE_WIDTH;
	const int32_t MAP_HEIGHT = MAP_HEIGHT_T * TILE_HEIGHT;
}

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_constants.h
///-------------------------------------------------------------------------------------------------


