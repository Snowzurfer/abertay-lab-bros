///-------------------------------------------------------------------------------------------------
// file:	lb_gameover_gamestate.h
//
// summary:	Declares the gameover state class
///-------------------------------------------------------------------------------------------------

#ifndef	GAMEOVER_STATE_H
#define GAMEOVER_STATE_H

// Headers
#include <stdint.h>
#include <stdlib.h>
#include "gba.h"
#include "font.h"
#include "gba_exp.h"
#include "rect.h"
#include "gba_keys.h"
#include "lb_events.h"
#include "lb_constants.h"
#include "base_state.h"


class GameOver : public BaseState
{
	public:
		// Constructor
		GameOver(int32_t pScore, int32_t pLevel);

		// Destructor
		~GameOver(void);

		// Handle input
		void handleEvents();

		// Update the game logic
		void update();

		// Renders the graphics
		void render();

		// Load media
		bool loadMedia();

	private:
		
		// String buffer for printing text
		char *_stringBuffer;
		
		// Reference to key handler
		GBAKeys *_keysHandler;
};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_gameover_gamestate.h
///-------------------------------------------------------------------------------------------------

