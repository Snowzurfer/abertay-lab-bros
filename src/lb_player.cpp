

// Headers
#include "lb_player.h"

Player::Player(int32_t xPos, int32_t yPos): 
				Entity(xPos, yPos, 1, ENT_PLAYER), 
				_attackTimerTreshold(30), //Attack
				_attackTimerThresholdSword(20), // Sword spawn
				_hurtTimerTreshold(50), // Hurt
				_moveAnimTimerThreshold(10), // Moving anim
				_graphicsPos(Vector2D(xPos - 8, yPos - 8)){ // Pos of graphs
	
	_keysHandler = GBAKeys::Instance();
	
	_hurtTimerCounter = 0;
	_attackTimerCounter = 0;
	_movAnimCounter = 0;
	
	_currAnimFrame = 0;
	
	_score = 0;
	
	_attacked = false;
	
	++ objNumCounter;
}

Player::~Player() {
	//delete _spriteTileNum;
	//_spriteTileNum = NULL;
	//delete _anim;
}

void Player::draw(const Vector2D &camOffset) {
	_graphicsPos.x = (_pos.x - 8) - camOffset.x;
	_graphicsPos.y = (_pos.y - 8) - camOffset.y;
	
	// The entity is outside the boundaries of the screen
	if(_graphicsPos.x >= Const::RENDERAREA_W ||
		_graphicsPos.x <= 0 || 
		_graphicsPos.y >= Const::RENDERAREA_H ||
		_graphicsPos.y <= 0) {
		// Hide it
		if(_visible) {
			setObjectVisibility(_objNum, false);
			_visible = false;
		}
		setObjectVisibility(_objNum, false);
	}
	else {
		if(!_visible) {
			setObjectVisibility(_objNum, true);
			_visible = true;
		}
		
		// Set sprite's position relative to the camera
		SetObjectX(_objNum, _graphicsPos.x);
		SetObjectY(_objNum, _graphicsPos.y);
		
		// Set the correct frame (eventual flipping happens in update)
		setObjectTileIndex(_objNum, _currAnimFrame);
	}
	
}

void Player::handleTileCollision(int32_t tileType, int32_t tileIndex, int32_t w, int32_t h) {

	// Act differently depending on the type of tile
	switch(tileType) {
		case Const::TILE_STAIRS: {
			// Set the game to proceed to next dungeon level
			Events *eH = Events::Instance();
			eH->dispatchEvent(Event(Const::EVENT_CHANGELEVEL));
			break;
		}
		default: {
			
			// Act differently depending on the index of tile being computed
			switch(tileIndex) {
				case 0: { // Tile beneath the entity
					setDesPos(Vector2D(_desPos.x, _desPos.y - h));
					break;
				}
				case 1: { // Tile above entity
					setDesPos(Vector2D(_desPos.x, _desPos.y + h));
		
					break;
				}
				case 2: { // Tile left of entity
					setDesPos(Vector2D(_desPos.x + w, _desPos.y));
					
					break;
				}
				case 3: { // Tile right of entity
					setDesPos(Vector2D(_desPos.x - w, _desPos.y));
					
					break;
				}
				default: { // Handle diagonal tiles cases separately
					// If the intersection rectangle is wider than taller
					if(w > h) { 
						// Resolve vertically
						
						// If the tile is beneath the entity
						if(tileIndex > 5) {
							setDesPos(Vector2D(_desPos.x, _desPos.y - h));
						}
						// If the tile is above the entity
						else {
							setDesPos(Vector2D(_desPos.x, _desPos.y + h));
						}
					}
					// If the intersection rectangle is taller than wider
					else {
						// Resolve horizontally
						
						// If the tile is left to the entity
						if(tileIndex == 6 || tileIndex == 4){
							setDesPos(Vector2D(_desPos.x + w, _desPos.y));
						}
						// If the tile is right to the entity
						else {
							setDesPos(Vector2D(_desPos.x - w, _desPos.y));
						}
					}
				}
			}
		}
	}
}

void Player::handleEntityCollision(Entity *e) {
	// React differently depending on type of entity
	if(e->getType() == ENT_ENEMY_A || e->getType() == ENT_ENEMY_B) {
		
		// Get entity's bounding box. Since its position might change between one collision
		// check and the other, it is necessary to retrieve it every loop
		Rect *eABB = getBoundingBox();
			
		// Get entity's bounding box. Since its position might change between one collision
		// check and the other, it is necessary to retrieve it every loop
		Rect *eBBB = e->getBoundingBox();
		
		// Get the sign of the velocity of the entity being checked
		int32_t xVelSigNum = sigNum(_velocity.x);
		int32_t yVelSigNum = sigNum(_velocity.y);
		
		// Get the intersection rectangle (area between the two entities)
		int32_t x = fmax(eABB->pos.x, eBBB->pos.x);
		int32_t y = fmax(eABB->pos.y, eBBB->pos.y);
		int32_t w = fmin(eABB->pos.x + eABB->w, eBBB->pos.x + eBBB->w);
		int32_t h = fmin(eABB->pos.y + eABB->h, eBBB->pos.y + eBBB->h);
		w-=x;
		h-=y;
		
		// Calculate new desired position of the entity
		int32_t xPos = _desPos.x - (w * xVelSigNum);
		int32_t yPos = _desPos.y - (h * yVelSigNum);
		
		// If the intersection rectangle is wider than taller
		if(w > h) { 
			// Resolve vertically	
			setDesPos(Vector2D(_desPos.x, yPos));	
		}
		// If the intersection rectangle is taller than wider
		else {
			// Resolve horizontally	
			setDesPos(Vector2D(xPos, _desPos.y));
		}
		
		// Start hurt routine
		if(_currState == STATE_IDLE || _currState == STATE_MOVING ||
			_currState == STATE_ATTACK) {
	
			// Reduce health
			-- _health;
	
			// Stop the player
			_velocity.x = 0;
			_velocity.y = 0;
			
			// Set next state
			_currState = STATE_HURT;
			
			// Reset timer
			_hurtTimerCounter = 0;
		}
	}
}

void Player::update() {
	// Update current state
	switch(_currState) {
		case STATE_ATTACK: {
			// If the attack has not been started
			if(!_attacked) {
				
				
				// Reference to events handler
				Events *eH = Events::Instance();
				
				// Dispatch event for spawning the sword
				// (Tell the engine to spawn the sword)
				eH->dispatchEvent(Event(Const::EVENT_SPAWNSWORD));
				
				// Set the flag to true
				_attacked = true;
				
				// Stop the player
				_velocity.x = 0;
				_velocity.y = 0;
				
				// Start the timer for the player's state
				_attackTimerCounter = 0;
				
				// Set animation depending on direction
				switch(_currDir) {
					case DIR_NORTH: {
						_currAnimFrame = Const::FRM_PLAYER_N_ATK;
						setObjectFlipY(_objNum, false);
						break;
					}
					case DIR_EAST: {
						_currAnimFrame = Const::FRM_PLAYER_E_ATK;
						setObjectFlipY(_objNum, false);
						break;
					}
					case DIR_WEST: {
						_currAnimFrame = Const::FRM_PLAYER_W_ATK;
						setObjectFlipY(_objNum, true);
						break;
					}
					case DIR_SOUTH: {
						_currAnimFrame = Const::FRM_PLAYER_S_ATK;
						setObjectFlipY(_objNum, false);
						break;
					}
				}
			}
		
			// Update timer
			++_attackTimerCounter;
			
			
			// Check if it's time to spawn the sword
			/*if(_attackTimerCounter == _attackTimerThresholdSword) {
				// Spawn sword
				// Reference to events handler
				Events *eH = Events::Instance();
				
				// Dispatch event for spawning the sword
				eH->dispatchEvent(EVENT_SPAWNSWORD);
			}*/
			
			// Check for threshold for finishing attack routine
			if(_attackTimerCounter >= _attackTimerTreshold) {
				// Exit this state and set it to idle
				_currState = STATE_IDLE;
				
				// Reference to events handler
				Events *eH = Events::Instance();
				
				// Dispatch event for deleting the sword
				eH->dispatchEvent(Event(Const::EVENT_DELETESWORD));
				
				// Reset the timer
				_attackTimerCounter = 0;
				
				// Reset the flag
				_attacked = false;
			}
			
			break;
		}
		case STATE_IDLE: {
			
			// If the player wants to attack
			if(_attack) {
				_prevState = _currState;
				_currState = STATE_ATTACK;
			}
			
			// If the player wants to move
			if(_moveRight || _moveLeft || _moveUp || _moveDown) {
				// Set state to move
				_prevState = _currState;
				_currState = STATE_MOVING;
			}
			
			
			
			// Set animation depending on direction
			switch(_currDir) {
				case DIR_NORTH: {
					_currAnimFrame = Const::FRM_PLAYER_N_IDLE;
					setObjectFlipY(_objNum, false);
					break;
				}
				case DIR_EAST: {
					_currAnimFrame = Const::FRM_PLAYER_E_IDLE;
					setObjectFlipY(_objNum, false);
					break;
				}
				case DIR_WEST: {
					_currAnimFrame = Const::FRM_PLAYER_W_IDLE;
					setObjectFlipY(_objNum, true);
					break;
				}
				case DIR_SOUTH: {
					_currAnimFrame = Const::FRM_PLAYER_S_IDLE;
					setObjectFlipY(_objNum, false);
					break;
				}
			}
		
			break;
		}
		case STATE_HURT: {
			// Update timer
			++_hurtTimerCounter;
			
			// Animate (make player flicker)
			
			
			// Check for threshold for finishing hurt routine
			if(_hurtTimerCounter >= _hurtTimerTreshold) {
				// Exit this state and set it to idle
				_currState = STATE_IDLE;
				
				// Reset timer
				_hurtTimerCounter = 0;
			}
			break;
		}
		case STATE_MOVING: {
			// If the player wants to attack
			if(_attack) {
				_prevState = _currState;
				_currState = STATE_ATTACK;
			}
			
			if (_moveUp && _moveDown) {
				// do nothing
			}
			else if (_moveUp) {
				_velocity.y = -_maxVel;
			}
			else if (_moveDown) {
				_velocity.y = _maxVel;
			}
			
			
			if (_moveLeft && _moveRight) {
				// do nothing
			}
			else if (_moveLeft) {
				_velocity.x = -_maxVel;
			}
			else if (_moveRight) {
				_velocity.x = _maxVel;
			}
			
			// Update anim
			
			// Inc counter
			++ _movAnimCounter;
			
			// If counter reached threshold
			if(_movAnimCounter % _moveAnimTimerThreshold == 0) {
				// Reset counter
				_movAnimCounter = 0;
				
				// Move to next frame
				switch(_currDir) {
					case DIR_NORTH: {
						// Choose next frame depending on current one
						if(_currAnimFrame == Const::FRM_PLAYER_N_WALK0) {
							_currAnimFrame = Const::FRM_PLAYER_N_WALK1;
						}
						else {
							_currAnimFrame = Const::FRM_PLAYER_N_WALK0;
						}
						
						setObjectFlipY(_objNum, false);
						
						break;
					}
					case DIR_EAST: {
						// Choose next frame depending on current one
						if(_currAnimFrame == Const::FRM_PLAYER_E_WALK0) {
							_currAnimFrame = Const::FRM_PLAYER_E_WALK1;
						}
						else {
							_currAnimFrame = Const::FRM_PLAYER_E_WALK0;
						}
						
						setObjectFlipY(_objNum, false);
						
						break;
					}
					case DIR_WEST: {
						// Choose next frame depending on current one
						if(_currAnimFrame == Const::FRM_PLAYER_W_WALK0) {
							_currAnimFrame = Const::FRM_PLAYER_W_WALK1;
						}
						else {
							_currAnimFrame = Const::FRM_PLAYER_W_WALK0;
						}
						
						setObjectFlipY(_objNum, true);
						
						break;
					}
					case DIR_SOUTH: {
						// Choose next frame depending on current one
						if(_currAnimFrame == Const::FRM_PLAYER_S_WALK0) {
							_currAnimFrame = Const::FRM_PLAYER_S_WALK1;
						}
						else {
							_currAnimFrame = Const::FRM_PLAYER_S_WALK0;
						}
						
						setObjectFlipY(_objNum, false);
						
						break;
					}
				}
			}
			break;
		}
		case STATE_DEAD: {
			// Let the engine know
			// Reference to events handler
			Events *eH = Events::Instance();
			
			// Dispatch event for deleting the sword
			eH->dispatchEvent(Event(Const::EVENT_PLAYERDEAD));
			
			/*char stringBuffer[100];
							
				sprintf(stringBuffer, "LOOOOL");
				drawText(10, 4, stringBuffer, Const::SB_TEXT);*/
			
			break;
		}
	
	}
	
	// Update base class
	Entity::update();
}

void Player::handleInput() {
	// Get the current state of the buttons.
	_keysHandler->updateCurrent();

	
	// Check user input and behave accordingly
	char stringBuffer[100];
	
	
	
	// Movement
	if(_keysHandler->isBPressed(KEY_UP)) {
		_currDir = DIR_NORTH;
		_moveUp = true;
	}
	else {
		_moveUp = false;
	}
	
	if(_keysHandler->isBPressed(KEY_DOWN)) {
		_currDir = DIR_SOUTH;
		_moveDown = true;
	}
	else {
		_moveDown = false;
	}
	
	if(_keysHandler->isBPressed(KEY_LEFT)) {
		_currDir = DIR_WEST;
		_moveLeft = true;
	}
	else {
		_moveLeft = false;
	}
	
	if(_keysHandler->isBPressed(KEY_RIGHT)) {
		_currDir = DIR_EAST;
		_moveRight = true;
	}
	else {
		_moveRight = false;
	}
	
	
	// Attack
	if(_keysHandler->isBPressed(KEY_A)) {
		_attack = true;
	}
	else {
		_attack = false;
	}
	
	
	// Update previous buttons variable
	_keysHandler->updatePrevious();
}

void Player::setupObj() {
	// as soon as the main loop starts everything is
	// set correctly.
	SetObject(_objNum,
	          ATTR0_SHAPE(0) | ATTR0_8BPP | ATTR0_REG | ATTR0_Y(_graphicsPos.y),
			  ATTR1_SIZE(1) | ATTR1_X(_graphicsPos.x),
			  ATTR2_ID8(_currAnimFrame));
}