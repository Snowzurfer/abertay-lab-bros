
//{{BLOCK(ent_sprites)

//======================================================================
//
//	ent_sprites, 128x64@8, 
//	Transparent palette entry: 3.
//	+ palette 16 entries, not compressed
//	+ 128 tiles not compressed
//	Total size: 32 + 8192 = 8224
//
//	Time-stamp: 2014-05-17, 16:49:26
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.6
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_ENT_SPRITES_H
#define GRIT_ENT_SPRITES_H

#define ent_spritesTilesLen 8192
extern const unsigned short ent_spritesTiles[4096];

#define ent_spritesPalLen 32
extern const unsigned short ent_spritesPal[16];

#endif // GRIT_ENT_SPRITES_H

//}}BLOCK(ent_sprites)
