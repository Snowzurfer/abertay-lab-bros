///-------------------------------------------------------------------------------------------------
// file:	lb_baseentity.h
//
// summary:	BaseEntity represents the starting point for the entities in the game and 
//			holds attributes and functions shared among all of the possible entities.
///-------------------------------------------------------------------------------------------------

#ifndef LBBASEENTITY_H
#define LBBASEENTITY_H


// Headers
#include "vector_2d.h"
#include <stdint.h>
#include "gba.h"
#include "gba_exp.h"
#include "rect.h"
#include "lb_events.h"
#include "lb_constants.h"

// Possible sates of an entity
enum EntityStates {
	STATE_IDLE = 0,
	STATE_MOVING,
	STATE_ATTACK,
	STATE_HURT,
	STATE_DEAD
};

// Facing direction
enum FacingDir {
	DIR_NORTH = 0,
	DIR_SOUTH,
	DIR_WEST,
	DIR_EAST
};

// Different type of entities
enum EntityType {
	ENT_PLAYER = 0,
	ENT_ENEMY_A,
	ENT_ENEMY_B,
	ENT_SWORD
};


class Entity
{
	public:
	
		// Constructor
		Entity(int32_t xPos, int32_t yPos, int32_t vel, EntityType eType);

		// Destructor
		virtual ~Entity();

		// Update the entity
		virtual void update();
		
		// Set the entity's GBA object relative to the camera and set the current frame
		virtual void draw(const Vector2D &camOffset);

		// Handle user input
		virtual void handleInput();
		
		// Called by the engine for letting the entity react
		virtual void handleEntityCollision(Entity *e);
		
		// Handle collision with tile
		virtual void handleTileCollision(int32_t tileType, int32_t tileIndex, int32_t w, int32_t h);
		
		// Set up OBJ attributes for this entity
		virtual void _setupObj();
		
		// Set one direction only
		void setOneDir(FacingDir dir);
		
		// Reduce the health for a certain amount
		void reduceHealth(int32_t amount);
		
		
		// Getters and setters
		
		const Vector2D& getDesPos(void) const		{ return(_desPos);};
		void setDesPos(const Vector2D& desPos)	{ _desPos = desPos;	};
		
		const Vector2D& getPos(void) const		{ return(_pos);};
		void setPos(const Vector2D& pos)	{ _pos = pos;	};
		
		const Vector2D& getVel(void) const		{ return(_velocity);};
		void setVel(const Vector2D& vel)	{ _velocity = vel;	};
		
		const bool& isAlive(void) const		{ return(_alive);};
		void setAlive(const bool& alive)	{ _alive = alive;	};
	
		const int32_t& getObjNum(void) const		{ return(_objNum);};
		void setObjNum(const int32_t& objNum)	{ _objNum = objNum;	};
		
		const EntityStates& getCurrState(void) const		{ return(_currState);};
		void setCurrState(const EntityStates& currState)	{ _currState = currState;	};
		
		const EntityType& getType(void) const		{ return(_type);};
		
		const int32_t& getHealth(void) const		{ return(_health);};
		
		const int32_t &getMaxVel(void) const { return (_maxVel);};
		
		const FacingDir &getCurrDir(void) const { return (_currDir); };
		
		const int32_t &getScore(void) const { return _score;};
		void setScore(const int32_t &score) { _score = score;};
		
		Rect *getBoundingBox(void);
		
		
		// Score of the entity
		int32_t _score;
		
		// Holds the latest obj number being spawned
		static int32_t objNumCounter;
		
	protected:
		
		// Desired position of the entity
		Vector2D _desPos;
		
		// Real position of the entity
		Vector2D _pos;
		
		// Used to get input and know which buttons have been pressed the current frame
		// (FUTURE IMPROVEMENT: USE BITFIELDS AND SUBSTITUTE ALL THESE BOOLS
		// WITH AN INTEGER)
		bool _moveRight;
		bool _moveLeft;
		bool _moveUp;
		bool _moveDown;
		bool _attack;
		
		bool _collMap;
		
		// Velocity of the entity
		Vector2D _velocity;
		
		// Health of the entity
		int32_t _health;
		
		// If the entity is alive or not (in which case an action will be taken)
		bool _alive;
		
		// Type of entity
		const EntityType _type;
		
		// If the entity has to be rendered or not
		bool _visible;
		
		// Max velocity
		const int32_t _maxVel;
		
		// ID number to identify the entity's object
		int32_t _objNum;
		
		// Current direction the entity is facing
		FacingDir _currDir;
		// Last direction the entity was facing (used for animation change)
		FacingDir _prevDir;
		// Whether the entity has changed direction in the last frame
		bool _dirChange;
		
		// Used to determine the bounding box of the entity (collisions ;-) )
		Rect *_boundingBox;
		
		// Current state of the entity
		EntityStates _currState;
		
		// Previous state of the entity
		EntityStates _prevState;
		
		
		
		// When called, stops the entity
		void _stopMoving();
};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_baseentity.h
///-------------------------------------------------------------------------------------------------
