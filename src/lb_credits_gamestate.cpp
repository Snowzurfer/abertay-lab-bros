///-------------------------------------------------------------------------------------------------
// file:	lb_credits_gamestate.cpp
//
// summary:	Source file of credits state
///-------------------------------------------------------------------------------------------------

// Headers
#include "lb_credits_gamestate.h"


Credits::Credits(void) {
	
	_stringBuffer = new char[100];
	
	_keysHandler = GBAKeys::Instance();
	
	// Credits text
	sprintf(_stringBuffer, "A game by Alberto Taiuti");
	drawText(3, 4, _stringBuffer, Const::SB_TEXT);
	sprintf(_stringBuffer, "1300250 - CGT Abertay Dundee");
	drawText(1, 6, _stringBuffer, Const::SB_TEXT);
	sprintf(_stringBuffer, "Mod. Computer & Graphics");
	drawText(2, 10, _stringBuffer, Const::SB_TEXT);
	sprintf(_stringBuffer, "Architecture");
	drawText(6, 11, _stringBuffer, Const::SB_TEXT);
	sprintf(_stringBuffer, "Academic year 2013 / 2014");
	drawText(3, 15, _stringBuffer, Const::SB_TEXT);
}

Credits::~Credits(void) {
	delete [] _stringBuffer;
}

void Credits::handleEvents() {
	// Get the current state of the buttons.
	_keysHandler->updateCurrent();


	if(_keysHandler->isBJustPressed(KEY_A)) {
		// Dispatch change state event
			
		std::vector<int32_t> args;
		args.push_back(Const::STATE_MENU);
		eH->dispatchEvent(GameStateEvent(Const::EVENT_CHANGESTATE, args));
	}
	
	// Update previous buttons variable
	_keysHandler->updatePrevious();
}

// Update is called constantly
void Credits::update() {
	
	
}

void Credits::render() {

}

// Load assets
bool Credits::loadMedia() {

	clearScreenBlock32(Const::SB_BACKGROUND, 1);
}

///-------------------------------------------------------------------------------------------------
// End of lb_credits_gamestate.cpp
///-------------------------------------------------------------------------------------------------