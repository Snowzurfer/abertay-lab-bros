///-------------------------------------------------------------------------------------------------
// file:	lb_dungeon.h
//
// summary:	Declares the dungeon class
///-------------------------------------------------------------------------------------------------

#ifndef LBDUNGEON_H
#define LBDUNGEON_H

// Headers
#include <stdint.h>
#include <vector>
#include <math.h>
#include <cstdlib>
#include "lb_room.h"
#include "lb_tile.h"
#include "gba_exp.h"
#include "lb_player.h"
#include "lb_enemyA.h"
#include "lb_enemyB.h"
#include "lb_constants.h"


// Directions
const int32_t NORTH_DIR = 0;
const int32_t SOUTH_DIR = 2;
const int32_t EAST_DIR = 1;
const int32_t WEST_DIR = 3;

const int32_t MAP_WIDTH_T = 25;
const int32_t MAP_HEIGHT_T = 25;

const int32_t ENT_NUM = 6;

class Dungeon 
{
	public:
		
		// Constructor
		Dungeon();
		
		// Destructor
		~Dungeon();

		/**
		 *  Creates the map and returns the player with the position set randomly
		 *  plus the entities populating the level
		 */
		std::vector<Entity*> *makeMap(int32_t diffLevel, int32_t seed, Player* player);

	
		// Array of tiles composing the dungeon
		int32_t *dungeonMap;
		
		// Return the position in tiles of a given point
		static Vector2D getTileCoordFromPixel(const Vector2D &pV);
		
		// Return an array containing the position in the tiles array of the
		// tiles around a given point (used in collision detection)
		int32_t *getSurroundingTilesIndexes(const Vector2D &pV);
		
	private :
		
		// Max number of rooms in dungeon
		int32_t maxRooms;

		// Used to seed the rand function
		uint32_t seedIncrement;
 
		// Available tiles
		enum {
			tileUnused = 0,
			tileDirtWall,
			tileDirtFloor,
			tileStoneWall,
			tileCorridor,
			tileDoor,
			tileUpStairs,
			tileDownStairs,
			tileChest
		};


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Dig a room in the array of keys. </summary>
		///
		/// <remarks>	Alberto, 18/04/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		void digRoom(const Room *r);


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Dig vertical corridor. </summary>
		///
		/// <remarks>	Alberto, 18/04/2014. </remarks>
		///
		/// <param name="x1">		 	The first x value. </param>
		/// <param name="x2">		 	The second x value. </param>
		/// <param name="y">		 	The y poition of the corridor. </param>
		/// <param name="dungeonMap">	[in,out] If non-null, the dungeon map. </param>
		///-------------------------------------------------------------------------------------------------
		void digVCorridor(int32_t y1, int32_t y2, int32_t x);


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Dig horizontal corridor. </summary>
		///
		/// <remarks>	Alberto, 18/04/2014. </remarks>
		///
		/// <param name="y1">		 	The first y value. </param>
		/// <param name="y2">		 	The second y value. </param>
		/// <param name="x">		 	The x poition of the corridor. </param>
		/// <param name="dungeonMap">	[in,out] If non-null, the dungeon map. </param>
		///-------------------------------------------------------------------------------------------------
		void digHCorridor(int32_t x1, int32_t x2, int32_t y);
		
		
		///-------------------------------------------------------------------------------------------------
		/// <summary>	Gets a random number in a range. </summary>
		///
		/// <remarks>	Alberto, 20/04/2014. </remarks>
		///
		/// <param name="min">	The minimum. </param>
		/// <param name="max">	The maximum. </param>
		///
		/// <returns>	The random number. </returns>
		///-------------------------------------------------------------------------------------------------
		int32_t getRand(int32_t min, int32_t max);
		
		// Set a specific tile in the map
		void setSingleTileInRoom(const Room *r, int32_t tXPos, int32_t tYPos, int32_t tType);

};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_dungeon.h
///-------------------------------------------------------------------------------------------------