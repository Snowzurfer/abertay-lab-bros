/**
 *  Rect.cpp
 *  Source file of the Rect class.
 *  
 *  Contains the definition of member functions.
 *  
 *  Project: Abertay Lab Bros: Quest For Oculus
 *  Author: Alberto Taiuti, MMXIII 
 */ 

// Included libs
#include "rect.h"

Rect::Rect() {
	pos = Vector2D(0, 0);
	w = h = 1;
}

Rect::Rect(const Rect &obj) {
	pos = obj.pos;
	w = obj.w;
	h = obj.h;
}

Rect::Rect(int32_t xPos, int32_t yPos, int32_t _width, int32_t _height) {
	pos = Vector2D(xPos, yPos);
	w = _width;
	h = _height;
}

Rect::~Rect()
{
	// Delete pouint16_t instances
	//pouint16_ts.clear();
}

bool Rect::checkCollision(const Rect &A, const Rect &B) {
    //The sides of the rectangles
    int32_t leftA, leftB;
    int32_t rightA, rightB;
    int32_t topA, topB;
    int32_t bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = A.pos.x;
    rightA = A.pos.x + A.w;
    topA = A.pos.y;
    bottomA = A.pos.y + A.h;

    //Calculate the sides of rect B
    leftB = B.pos.x;
    rightB = B.pos.x + B.w;
    topB = B.pos.y;
    bottomB = B.pos.y + B.h;

    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }

    if( topA >= bottomB )
    {
        return false;
    }

    if( rightA <= leftB )
    {
        return false;
    }

    if( leftA >= rightB )
    {
        return false;
    }

    //If none of the sides from A are outside B
    return true;
}

Rect *Rect::getIntersectionRect(const Rect &A, const Rect &B) {
	int32_t x = fmax(A.pos.x, B.pos.x);
	int32_t y = fmax(A.pos.y, B.pos.y);
	int32_t w = fmin(A.pos.x + A.w, B.pos.x + B.w);
	int32_t h = fmin(A.pos.y + A.h, B.pos.y + B.h);
	
	Rect *iR = new Rect(x, y, w - x, h - y);
	
	return iR;
}

