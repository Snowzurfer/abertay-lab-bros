// Animation class

#ifndef LBANIM_H
#define LBANIM_H

// Headers
#include <stdint.h>
#include "gba.h"
#include "gba_exp.h"

class Anim
{
	public:

		
		Anim(int32_t framesNum, int32_t objNum, bool flipV, bool flipH, int32_t framesTreshold);


		///-------------------------------------------------------------------------------------------------
		/// <summary>	Destructor. </summary>
		///
		/// <remarks>	Alberto, 18/04/2014. </remarks>
		///-------------------------------------------------------------------------------------------------
		~Anim();
		
		// Update the animation
		void update();
		
		// Change the animation 
		void draw();

		// Add one frame to the animation by telling it the position in the OAM
		void pushFrame(int32_t framePos);

	private:

		// Number of frames per animation
		const int32_t _framesNum;
		
		// Number of the sprite
		const int32_t _objNum;
		
		const bool _flipH;
		
		const bool _flipV;
		
		// Position of frames in the OAM
		int32_t *_framesPos;
		
		// Current frame of animation being displayed
		int32_t _currAnimFrame;
		
		// Increases every time the update is called
		int32_t _framesCounter;
		
		// Whether it's time to change the frame
		bool _changeFrame;
		
		// Determines the speed at which the animation will change the frame
		const int32_t _framesTreshold;
};

#endif

///-------------------------------------------------------------------------------------------------
// End of room.h
///-------------------------------------------------------------------------------------------------
