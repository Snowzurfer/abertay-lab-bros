///-------------------------------------------------------------------------------------------------
// file:	lb_gameover_gamestate.cpp
//
// summary:	Implements the gameover state class
///-------------------------------------------------------------------------------------------------


// Headers
#include "lb_gameover_gamestate.h"


GameOver::GameOver(int32_t pScore, int32_t pLevel) {
	
	_stringBuffer = new char[100];
	
	_keysHandler = GBAKeys::Instance();
	
	// GameOver text
	sprintf(_stringBuffer, "GAME OVER!");
	drawText(10, 4, _stringBuffer, Const::SB_TEXT);
	sprintf(_stringBuffer, "Your score:   %d", pScore);
	drawText(8, 8, _stringBuffer, Const::SB_TEXT);
	sprintf(_stringBuffer, "Level reached:%d", pLevel);
	drawText(8, 10, _stringBuffer, Const::SB_TEXT);
	sprintf(_stringBuffer, "Press A");
	drawText(11, 16, _stringBuffer, Const::SB_TEXT);
	
}

GameOver::~GameOver(void) {
	delete [] _stringBuffer;
}

void GameOver::handleEvents() {
	// Get the current state of the buttons.
	_keysHandler->updateCurrent();

	if(_keysHandler->isBJustPressed(KEY_A)) {
		// Dispatch change state event
		std::vector<int32_t> args;
		args.push_back(Const::STATE_MENU);
		eH->dispatchEvent(GameStateEvent(Const::EVENT_CHANGESTATE, args));
	}
	
	// Update previous buttons variable
	_keysHandler->updatePrevious();
}

// Update is called constantly
void GameOver::update() {
	
	
}

void GameOver::render() {

}

// Load assets
bool GameOver::loadMedia() {

	clearScreenBlock32(Const::SB_BACKGROUND, 1);
}

///-------------------------------------------------------------------------------------------------
// End of lb_gameover_gamestate.cpp
///-------------------------------------------------------------------------------------------------