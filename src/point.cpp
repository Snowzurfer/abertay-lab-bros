/**
 *  Point.cpp
 *  Source file of the Point class.
 *  
 *  Contains the definition of member functions
 *  
 *  Project: Abertay Lab Bros: Quest For Oculus
 *  Author: Alberto Taiuti, MMXIII 
 */ 

// Included libs
#include "Point.h"

Point::Point()
{
	x = 0; y = 0;
}

Point::Point(int32_t x, int32_t y)
{
	this->x = x; this->y = y;
}

Point::~Point(){}

/*void Point::rotate2D(int32_t angle)
{
	// Store the trigs of the angle, to save CPU (they won't be computed again)
	int32_t cos_angle = fixedpoint::cos(f16(angle));
	int32_t sin_angle = fixedpoint::sin(f16(angle));
	
	// Rotate the position around the origin
	int32_t temp_x = x*cos_angle - y*sin_angle;
	int32_t temp_y = x*sin_angle + y*cos_angle;
	
	// Assign values back
	x = temp_x; y = temp_y;
}*/

void Point::setPos(int32_t new_x, int32_t new_y)
{
	this->x = new_x; this->y = new_y;	
}

void Point::addToPos(int32_t x_step, int32_t y_step)
{
	// Add values
	x += x_step; y += y_step;
}

