/**
 *  Rect.h
 *  Header file of Rect class.
 *  
 *  The Rect class represents a geometric rectangle. It is made of 4 points
 *  (its vertex), stored under an std::vector container.
 *  
 *  Project: Abertay Lab Bros: Quest For Oculus
 *  Author: Alberto Taiuti, MMXIII 
 */ 

#ifndef RECT_H
#define RECT_H

// Included libs
#include "point.h"
#include "vector_2d.h"
#include <stdint.h>
#include <math.h>

// Class definition
class Rect
{
	public:
		
		// Default constructor
		Rect(); 
		// Copy constructor
		Rect(const Rect &obj); 
		
		Rect(int32_t xPos, int32_t yPos, int32_t _width, int32_t _height); 
		
		// Default deconstructor
		~Rect(); 
		
		// Position
		Vector2D pos;
	
		/**
		 *  Constants are used since they 
		 *  are stored into he external ROM, thus more
		 *  internal RAM free.
		 */
		int32_t w;
		int32_t h;
		
		// Getters for 
		/*int32_t x() { return _x;};
		int32_t y() { return _y;};
		int32_t w() { return _width;};
		int32_t h() { return _height;};*/
		
		static bool checkCollision(const Rect &A, const Rect &B);
		
		// Returns a ptr to a rect obj allocated in dynamic memory representing
		// the area of the intersection between A & B
		static Rect *getIntersectionRect(const Rect &A, const Rect &B);
		
	private:
	
		
		
};

#endif