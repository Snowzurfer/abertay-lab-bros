///-------------------------------------------------------------------------------------------------
// file:	lb_enemyB.h
//
// summary:	Declares the enemyB entity class
///-------------------------------------------------------------------------------------------------

#ifndef LBENEMYB_H
#define LBENEMYB_H


// Headers
#include "vector_2d.h"
#include <stdint.h>
#include "gba.h"
#include "lb_baseentity.h"
#include "gba_keys.h"
#include "lb_constants.h"



class EnemyB : public Entity
{
	public:
		EnemyB(int32_t xPos, int32_t yPos, int32_t health);

		~EnemyB();

		void update();
		
		void handleEntityCollision(Entity *e);
		
		void draw(const Vector2D &camOffset);
		
		void handleTileCollision(int32_t tileType, int32_t tileIndex, int32_t w, int32_t h);
		
	private:
		
		// Timer for the hurt state
		int32_t _hurtTimerCounter;
		const int32_t _hurtTimerTreshold;
		
		// Timer for looping through moving animations
		int32_t _movAnimCounter;
		const int32_t _moveAnimTimerThreshold;
		
		// Current frame of animation
		int32_t _currAnimFrame;
		
		// Position of graphics with respect to centre of logic position of the sprite
		Vector2D _graphicsPos;
		
		// Invert current direction
		void _changeDir();
};

#endif

///-------------------------------------------------------------------------------------------------
// End of lb_enemyB.h
///-------------------------------------------------------------------------------------------------

