Project developed in C++ for the GameBoy Advance as the second coursework for the module Computer and Graphics Architecture at Abertay Dundee, during my first year.

Abertay Lab Bros: Quest for Oculus is inspired by games such as the Zelda series and roguelikes and represents in fact a hybrid between the two genres.

Core mechanics:
•	Dungeons are procedurally generated at run-time
•	Attack and move in real time like Link in the Zelda series
•	Enemies spawn procedurally and depending on difficulty level
•	Ability to get a reward for defeating the enemies (increase score)
•	Smart use of available VRAM to implement bigger levels

Overall I was very satisfied with the results and of the procedural map generation system. The creation of this game further improved my awareness on the importance of efficient C++ code.
